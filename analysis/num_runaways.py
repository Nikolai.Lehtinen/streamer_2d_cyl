#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jan 29 18:28:50 2018

Calculate the number of runaway electrons (or at least 4 keV)

@author: nle003
"""

#%%
import os
import os.path
import numpy as np
import matplotlib.pyplot as plt
from my_utils import string, header, myfloat
from streamer_2d_cyl import simulation_load_barebones, rootdir
from trump.trump_2D import ea_plot, ea_plot_slice
from streamer_2d_cyl_analysis import RegionInfo, load_all_region_fields, load_t_array, \
    ea_plot_slice_lim, locate_children
from trump import c_dif, end
from streamer_2d_cyl import n_aver, n_avez, c_aver, c_avez

#%% A fit to Bakhov et al [2000] runaway frequency
def nu_bakhov(E):
    "A very simple function, will also work with ExtendedArray"
    return 10**(11-6*np.exp(-(E/1e6-25)/12))

if False:
    plt.figure(333)
    plt.clf()
    EE=np.linspace(24,40)*1e6
    plt.semilogy(EE/1e6,nu_bakhov(EE))
    plt.xlabel(r'$E$, MV/m')
    plt.ylabel(r'$\nu_r$, s$^{-1}$')
    plt.grid(True)
    plt.savefig('bakhov.pdf')
    
#%%
def get_Emax(rs):
    Emax1 = 0
    for r in rs:
        E = np.sqrt(n_aver(r.Ercr)**2+n_avez(r.Ezcz)**2)
        Emax1 = max(Emax1,np.max(E.arr))
    return Emax1
   

def get_bakhov_source(simulation,kplot1,kplot2,source,Emax):
    #ts = load_t_array(simulation)
    #Nplot = len(ts)
    #print('Nplot=',Nplot)
    #dts = np.diff(ts)
    #nruntot = np.zeros(ts.shape)
    #source = np.zeros(ts.shape)
    #Emax_arr = np.zeros(ts.shape)
    print(header('Integrating the source in',simulation.root,', please wait'))
    for kplot in range(kplot1,kplot2):
        if kplot%20==0: print(kplot,'/',kplot2)
        rs = load_all_region_fields(simulation,kplot) # needs simulation.E0
        if rs is None: continue
        locate_children(rs)
        source[kplot] = 0
        Emax1 = 0
        for r in rs:
            E = np.sqrt(n_aver(r.Ercr)**2+n_avez(r.Ezcz)**2)
            r.source = nu_bakhov(E)*r.Ne*(~r.is_child)
            source[kplot] += r.geom.integrate(r.source)
            Emax1 = max(Emax1,np.max(E.arr))
        Emax[kplot]=Emax1
    #return source,Emax

#%% Generic file-growing interface for large calculations
def read_grow_calculate(fname,control_name,control,names,calculation_fun):
    new={control_name: control}
    N = len(control)
    for name in names:
        new[name]=np.zeros(control.shape)
    if not os.path.isfile(fname):
        istart = 0
    else:
        print('Loading',fname)
        old = np.load(fname)
        istart = len(old[control_name])
        for name in names:
            assert name in old.files
            assert len(old[name])==istart
        assert (old[control_name][:istart]==control[:istart]).all()
    if istart>0:
        for name in names:
            new[name][:istart]=old[name]
    if istart<N:
        print(('Extending' if istart>0 else 'Creating'),fname)
        calculation_fun(simulation,istart,N,source,Emax)
        np.savez_compressed(fname,**new)
    elif istart>N:
        print(istart,N)
        raise Exception('something wrong')
    # No action required if kplot1==Nplot
    return simulation,ts,Emax,source
        
        

#%%
def read_bakhov(E0,adir):
    simulation = simulation_load_barebones(rootdir + adir)
    simulation.E0 = E0 # needed for the Bakhov source
    ts = load_t_array(simulation)
    Nplot = len(ts)
    source = np.zeros(ts.shape)
    Emax = np.zeros(ts.shape)
    bfname = rootdir + adir + '/bakhov.npz'
    if not os.path.isfile(bfname):
        kplot1 = 0
    else:
        print('Loading',bfname)
        tmp = np.load(bfname)
        source_old = tmp['source']
        ts_old = tmp['ts']
        Emax_old = tmp['Emax']
        kplot1 = len(ts_old)
        assert (ts_old[:kplot1]==ts[:kplot1]).all()
    if kplot1>0:
        source[:kplot1]=source_old[:]
        Emax[:kplot1]=Emax_old[:]
    if kplot1<Nplot:
        print(('Extending' if kplot1>0 else 'Creating'),bfname)
        get_bakhov_source(simulation,kplot1,Nplot,source,Emax)
        np.savez_compressed(bfname,ts=ts,Emax=Emax,source=source)
    elif kplot1>Nplot:
        print(kplot1,Nplot)
        raise Exception('something wrong')
    # No action required if kplot1==Nplot
    return simulation,ts,Emax,source
   
def add_source_line(source,ts,thelabel):
    tmp = np.where(source>1)[0]; i1=tmp[0]-1; i2=tmp[-1]+2
    line,=plt.semilogy(ts[i1:i2]/1e-9,source[i1:i2])
    #ntot = np.sum(source[1:]*np.diff(ts))
    plt.grid(True)
    smax = 10**np.ceil((np.log10(np.max(source))+1))
    line.set_label(string(thelabel,': total =',myfloat(source[-1])))
    return smax

#%%
#simulation1 = simulation_load_barebones(rootdir + '/euler_coarse/E_4MVm')
# There is no midpoint_fine_nodetach/E_4MVm
# REMOVED: euler_coarse/E_4_then_2_MVm
dirs = ['euler_coarse/E_4MVm','/midpoint_fine/E_4MVm',
        '/midpoint_fine_nodetach/E_4MVm',
        '/midpoint_fine/E_4to2MVm','/midpoint_fine_smallphoto_smallcr/E_4MVm']
#        '/midpoint_fine_almostnoion/E_4MVm']
simulations=[]
sources=[]
tss = []
Emaxs = []
E0s = [4e6,4e6,4e6,2e6,4e6] #,4e6]
for E0,adir in zip(E0s,dirs):
    simulation,ts,Emax,source=read_bakhov(E0,adir)
    simulations.append(simulation)
    sources.append(source)
    tss.append(ts)
    Emaxs.append(Emax)

#%%
iswitch = 1175
sources[3][:iswitch]=sources[1][:iswitch]
Emaxs[3][:iswitch]=Emaxs[1][:iswitch]

#%%
plt.figure(1)
plt.clf()
ntots = []
for source,ts in zip(sources,tss):
    ntot = np.hstack((0,np.cumsum(source[1:]*np.diff(ts))))
    ntots.append(ntot)
#smax0 =  add_source_line(ntots[0],tss[0],'4 MV/m (coarse)')
smax1 =  add_source_line(ntots[1],tss[1],'4 MV/m')
smax2 =  add_source_line(ntots[2],tss[2],'4 MV/m, no detachment')
smax3 =  add_source_line(ntots[3],tss[3],'4 MV/m lowered to 2 MV/m')
smax4 =  add_source_line(ntots[4],tss[4],'4 MV/m, low bg ioniz')
#smax5 =  add_source_line(ntots[5],tss[5],'4 MV/m, very low photo/no cr')
#plt.ylim(1e2,max(smax0,smax1,smax2,smax3,smax4))
plt.xlim(25,32)
plt.ylim(1,1e6)
plt.xlabel(r'$t$, ns')
plt.ylabel(r'$N_r(t)$, dimensionless')
plt.legend()
plt.savefig('runaways.pdf')

#%%
plt.figure(2)
plt.clf()
#plt.plot(tss[0]/1e-9,Emaxs[0]/1e6,label='4 MV/m (coarse)')
plt.plot(tss[1]/1e-9,Emaxs[1]/1e6,label='4 MV/m')
plt.plot(tss[2]/1e-9,Emaxs[2]/1e6,label='4 MV/m, no detachment')
plt.plot(tss[3]/1e-9,Emaxs[3]/1e6,label='4 MV/m lowered to 2 MV/m')
plt.plot(tss[4]/1e-9,Emaxs[4]/1e6,label='4 MV/m, low bg ioniz')
#plt.plot(tss[5]/1e-9,Emaxs[5]/1e6,label='4 MV/m, very low photo/no cr')
plt.grid(True)
plt.xlim(25,32)
plt.ylim(10,40)
plt.legend()
plt.xlabel(r'$t$, ns')
plt.ylabel(r'max $E$, MV/m')
plt.savefig('Emax.pdf')
