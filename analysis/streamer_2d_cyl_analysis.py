#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jan 29 18:21:05 2018

@author: nle003
"""

import os
import socket
import pickle
import numpy as np
np.set_printoptions(precision=3)
import matplotlib as mpl
import matplotlib.pyplot as plt
from my_utils import header, myfloat, string
import trump
from trump import c_dif, end
trump.DEBUGLEVEL=1
import streamer_2d_cyl
from streamer_2d_cyl import Simulation, Region, WARN, CC
from trump.trump_2D import ea_plot, ea_plot_slice
from trump.trump_saving import arr_to_geom, tuple_to_ea
from trump.extended_array import grid_stop
from trump.trump_periodic_view import PeriodicViewEA2D

class RegionInfo:
    def __init__(self,record):
        self.id,self.name,self.parent_name,self.rmax,self.zmin,self.zmax,self.dr,self.dz=record
        self.children=[]
        pass
    def __repr__(self):
        # - use instead of self.__dict__
        s = 'Region(id='+str(self.id)+', '+self.name+', parent='+self.parent_name\
            +', size=['+str(myfloat(self.rmax))+'x('+str(myfloat(self.zmin))+':'\
            +str(myfloat(self.zmax))+')], d=['+str(myfloat(self.dr))+'x'+str(myfloat(self.dz))+'])'
        return s


def load_regions(simulation,kplot):
    """Our goal is to avoid expensive creation of "Region"s but just load the data needed
    simulation.E0 must be set!!!"""
    # 1. Load the region info
    fname = simulation.root+'/dat/common/regions{:05d}.pkl'.format(kplot)
    try:
        with open(fname,'rb') as file:
            tmp = pickle.load(file)
    except FileNotFoundError:
        return None, None
    # Decypher the tree info
    region_infos=[]
    E0 = tmp[0]
    for record in tmp[3:]:
        region_info = RegionInfo(record)
        region_id,name,parent_name,rmax,zmin,zmax,dr,dz=record
        parent=None
        for r in region_infos:
            if region_info.parent_name==r.name:
                parent = r
                break
        region_info.parent = parent
        if parent is not None:
            parent.children.append(region_info)
        region_infos.append(region_info)
    # Calculate the region level
    for r in region_infos:
        p = r
        level = -1
        while p is not None:
            p = p.parent
            level += 1
        r.level = level
    #print(region_infos)
    return region_infos, E0

def load_region_fields(region,simulation,kplot,field_names=[]):
    npz = np.load(simulation.root + '/dat/' + region.name + '/res{:05d}.npz'.format(kplot))
    t = npz['t']
    try:
        region.geom = arr_to_geom(npz['geom'])
    except KeyError as e:
        print(npz.keys())
        raise e
    if len(field_names)==0:
        for key in npz.keys():
            if key[-4:]=='_arr':
                field_names.append(key[:-4])
        #print('Can load the following fields:',field_names)
    for fname in field_names:
        ea = tuple_to_ea(region.geom,npz[fname+'_info'],npz[fname+'_arr'])
        setattr(region, fname, ea)
    region.Ercr = -c_dif(region.phi,0) # at CX-points
    region.Ezcz = -c_dif(region.phi,1) + simulation.E0 # at CY-points
    return t

def load_all_region_fields(simulation,kplot):
    rs, E0 = load_regions(simulation,kplot)
    simulation.E0 = E0
    if not rs is None:
        field_names = []
        for r in rs:
            load_region_fields(r,simulation,kplot,field_names)
    return rs
    
def load_all_region_fields_old(simulation,kplot):
    """Our goal is to avoid expensive creation of "Region"s but just load the data needed
    simulation.E0 must be set!!!"""
    # 1. Load the region info
    fname = simulation.root+'/dat/common/regions{:05d}.pkl'.format(kplot)
    try:
        with open(fname,'rb') as file:
            tmp = pickle.load(file)
    except FileNotFoundError:
        return None
    # Decypher the tree info
    region_infos=[]
    for record in tmp[3:]:
        region_info = RegionInfo(record)
        region_id,name,parent_name,rmax,zmin,zmax,dr,dz=record
        parent=None
        for r in region_infos:
            if region_info.parent_name==r.name:
                parent = r
                break
        region_info.parent = parent
        if parent is not None:
            parent.children.append(region_info)
        region_infos.append(region_info)
    # Calculate the region level
    for r in region_infos:
        p = r
        level = -1
        while p is not None:
            p = p.parent
            level += 1
        r.level = level
    #print(region_infos)
    # 2. Load the variables we wish
    field_names = []
    t = None
    for r in region_infos:
        npz = np.load(simulation.root + '/dat/' + r.name + '/res{:05d}.npz'.format(kplot))
        if t is None:
            t = npz['t']
        try:
            r.geom = arr_to_geom(npz['geom'])
        except KeyError as e:
            print(npz.keys())
            raise e
        if len(field_names)==0:
            for key in npz.keys():
                if key[-4:]=='_arr':
                    field_names.append(key[:-4])
            #print('Can load the following fields:',field_names)
        for fname in field_names:
            ea = tuple_to_ea(r.geom,npz[fname+'_info'],npz[fname+'_arr'])
            setattr(r, fname, ea)
        r.Ercr = -c_dif(r.phi,0) # at CX-points
        r.Ezcz = -c_dif(r.phi,1) + simulation.E0 # at CY-points
    return region_infos

def load_t_array(simulation):
    thedir = simulation.root + '/dat/system/'
    flist = [f for f in os.listdir(thedir) if f[:4]=='drdz']
    flist.sort()
    Nplot = len(flist)
    assert Nplot == int(flist[-1][4:9])+1
    ts = np.zeros((Nplot,))
    for kplot in range(Nplot):
        #if kplot % 100 == 0:
        #    print(kplot)
        tmp = np.load(thedir + 'drdz{:05d}.npz'.format(kplot))
        ts[kplot]=tmp['t']
    return ts

#%%
def locate_children(rs):
    for r in rs:
        # Mark the children
        r.is_child = r.Ne.copy()
        r.is_child.alloc(np.bool)
        if r.parent is None:
            proxy = PeriodicViewEA2D(r.is_child,axis=1)
        else:
            proxy = r.is_child
        for rc in r.children:
            irmax = np.int(np.rint(grid_stop(rc.geom.gridr)/r.geom.gridr.delta))
            izmin = np.int(np.rint((rc.geom.gridz.start-r.geom.gridz.start)/r.geom.gridz.delta))
            izmax = np.int(np.rint((grid_stop(rc.geom.gridz)-r.geom.gridz.start)/r.geom.gridz.delta))
            if izmin>=izmax:
                # wrapping
                assert r.parent is None
                # special treatment
                izmax += proxy.period
            assert izmax>izmin
            proxy[0:irmax,izmin:izmax]=True

#%% The new plotting routines
def ea_plot_slice_lim(ea,ir=None,iz=None,scale=1,margin=0,plotargs=(),plotopts={}):
    if ir is None:
        if iz is None: raise Exception('bla!')
        # Plot alont r
        plt.plot(ea.x(0)[margin:-margin]/scale,ea[margin:end-margin,iz],*plotargs,**plotopts)
    else:
        if iz is not None: raise Exception('bla?')
        l2 = -margin if margin>0 else None
        plt.plot(ea.x(1)[margin:l2]/scale,ea[ir,margin:end-margin].T,*plotargs,**plotopts)
