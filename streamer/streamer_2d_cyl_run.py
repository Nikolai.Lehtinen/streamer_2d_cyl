#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov 30 10:13:28 2017

@author: nle003
"""

# The first thing to do: the environment
import computing_environment
import numpy as np
np.seterr(all='ignore')
np.set_printoptions(precision=3)
import matplotlib as mpl
if computing_environment.HPC_BATCH:
    mpl.use('Agg')
else:
    import IPython.utils.io
    logini = IPython.utils.io.Tee('streamer_2d_cyl_log.txt')
import matplotlib.pyplot as plt
import shutil
from my_utils import header, myfloat
import trump
trump.DEBUGLEVEL=2 # default: 1
import streamer_2d_cyl
import chemistry
import photoionization
from streamer_2d_cyl import Qerr, Simulation, Region, WARN, CC, region_drdz
import trump.trump_2D as trump_2D
import trump.trump_periodic_view as trump_periodic_view
import trump.trump_saving as trump_saving


def CopyToDir(fname,dirname,ext=''):
    "Avoid problems of shutil.copy with setting ownership and permissions"
    if fname is None:
        print(WARN,'The executable file is not saved in',dirname,'/*'+ext+', copy manually!!!')
    shortfname = fname.split('/')[-1]
    print('Copying',fname,'to',dirname,'as',shortfname + ext)
    shutil.copyfile(fname,dirname + '/' + shortfname + ext)

if computing_environment.HPC_BATCH:
    mpl.use('Agg')

#%%#################################################################################################
# Global settings that could not be saved into a "pickle"
simdir='revision/E_4MVm'
nresume = -1 # from which output to resume, -1 to start new calculation
def Efun(t):
    return 4e6
#    t1 = 2.8e-8; t2 = 2.83e-8
#    E1 = 4e6; E2 = 2e6
#    if t<=t1:
#        return E1
#    elif t<=t2:
#        return E1 + (E2-E1)*(t-t1)/(t2-t1)
#    else:
#        return E2
do_replot = False

try:
    print('Executable =',__file__)
except NameError:
    print('Executable = main')
    running_as_main = True
else:
    running_as_main = False

this_file = None if running_as_main else __file__
used_files = [trump_2D.__file__, trump_periodic_view.__file__, trump_saving.__file__,
              chemistry.__file__, photoionization.__file__,
              streamer_2d_cyl.__file__, this_file]

#%%#################################################################################################
# Load or set parameters
simulation_root = streamer_2d_cyl.rootdir + '/' + simdir
if (nresume >= 0):
    if not computing_environment.HPC_BATCH:
        logini.close()
        logfname = 'log_resume_'+str(nresume)+'.txt'
        logfile = IPython.utils.io.Tee(streamer_2d_cyl.rootdir + '/' + simdir + '/' + logfname)
    # Load parameters
    simulation = streamer_2d_cyl.simulation_load_barebones(simulation_root)
    # The following change is mandatory
    if computing_environment.HPC_BATCH:
        simulation.figsavefmts = ['png'] # do not clog with 6 MB files
    else:
        simulation.figsavefmts = ['pkl','png']
    #######################################
    # EMERGENCY CHANGES ARE INSERTED HERE #
    #simulation.Aphot = 0.1*60/(760+60)
    #simulation.Scro = 1e7 # or chemistry.Scro
    #simulation.all_global_plots = True    #
    #simulation.figsavefmts = ['png'] # do not clog with 6 MB files
    #simulation.Aphot = 0.1*30/(760+30)
    #simulation.Scro = 1e6 # or chemistry.Scro
    #simulation.interp_margin=1
    # Important to save the rest
    #simulation.save_barebones()
    # Alternatively, just rerun until the first output.
    # IMPORTANT WARNING: if re-running from beginning, SAVE log.txt, it gets overwritten!!!!!
    #######################################
    print(CC('blue')+'Loaded simulation',CC('end'))
    #system = simulation.regions[0]
    #detail = simulation.regions[1]
    ext = '.'+str(nresume)
    for f in used_files:
        is_different = simulation.diff_file(f)
        if is_different:
            CopyToDir(f,simulation.root,ext)
else:
    ################################################################################################
    # Part 1/2 of simulation creation
    # Setup the simulation preferences and parameters
    simulation = Simulation()
    simulation.root = simulation_root
    simulation.save_fig = True
    simulation.save_dat = True
    ################################################################################################
    # Saving preferences
    if simulation.save_dat or simulation.save_fig:
        existed = simulation.makedir('')
        if not computing_environment.HPC_BATCH:
            logini.close()
            logfile = IPython.utils.io.Tee(streamer_2d_cyl.rootdir + '/' + simdir + '/log.txt')
        # Save the simulation
        if existed:
            for f in used_files:
                simulation.diff_file(f)
        for f in used_files:
            CopyToDir(f,simulation.root)
    ################################################################################################
    # Part 2/2 of simulation creation
    simulation.round_error = 1e-4 # a check to align grid boundaries
    simulation.adv_extra = 2
    simulation.fringe = 2 # for sending fine info to super-region, must be >=1
    simulation.interp_margin = 1 # must be > (nls,nus)/refine, do not count bad points in this area
    simulation.adv_alg='ULTIMATE'
    simulation.operation_order = 'ion_tra' # default is 'ion_tra'
    # Photoionization setup, the best (fastest/accurate) is C[3]*=5, select_p=[3,4]
    # It is slightly higher overall but most accurate at all important distances
    simulation.Cphot = photoionization.C.copy()
    simulation.Lphot = photoionization.LamPhot.copy()
    simulation.Aphot = 0.1*60/(760+60) # default: 0.1*60/(760+60)
    simulation.Scro = 1e7 # default: 1e7 or chemistry.Scro
    #simulation.Cphot[3] *= 5
    simulation.select_p = range(2,5) # range(5)
    simulation.do_detachment = True # default is True
    simulation.second_order = True # choose euler or midpoint method
    simulation.nls = (3,3); simulation.nus = (3,3)
    simulation.do_photoionization = True
    simulation.do_diffusion = True
    simulation.do_explicit_diffusion = False
    # Factors <1 for time interval calculation
    simulation.alf = 0.9 # default is 0.9
    simulation.alfi = 0.25 # default is 0.25
    # if alfi is changed from 0.5 to 0.25, error must go down by
    # a factor 4 (1st order) or 8 (2nd order)
    # Error for alfi=0.5: midpoint/euler = .16; for alfi=.25:  midpoint/euler = 0.08
    # Error for alfi=0.5: euler = .15; midpoint = .024; for alfi=0.25: euler=.034; midpoint = .003
    if computing_environment.HPC_BATCH:
        simulation.figsavefmts = ['png'] # do not clog with 6 MB files
    else:
        simulation.figsavefmts = ['pkl','png']
    simulation.figcmap = mpl.cm.jet
    simulation.figscale = 0.001 # Plot stuff in mm
    simulation.all_global_plots = False
    simulation.all_plots = False # plots for E, dN
    simulation.adaptive = True # default is True
    if simulation.adaptive:
        simulation.marginsmall = np.array([5,5],dtype=np.int)
        simulation.marginbig = np.array([15,15],dtype=np.int)
        simulation.refine = np.array([5,5],dtype=np.int)
        # Choose betwee coarse and fine
        #simulation.badness_boundaries = np.array([1.,1.5,2.]) # coarse
        simulation.badness_boundaries = np.array([.5,.75,1.]) # fine
        simulation.max_depth = 2
    # Save barebones
    simulation.save_barebones()
    simulation.system = Region(simulation.next_region_id,simulation,'system',None,
                               rmax=2e-3,zmin=-7.5e-3,zmax=7.5e-3,dr=2e-5,dz=2e-5)
    assert len(simulation.regions)==0
    simulation.update_region_list()
    simulation.next_region_id += 1
pass

#%%#################################################################################################
# Initial values
if (nresume >= 0):
    if do_replot:
        print(WARN,'REPLOTTING!')
        for kprev in range(nresume+1):
            simulation.load_dat(kprev)
            for region in simulation.regions:
                region.plot()
    else:
        simulation.load_dat(nresume)
else:
    # Brand-new calculation
    simulation.t = 0
    simulation.tnext = 0
    simulation.kplot = 0
    Ne_ea = simulation.system.Ne.ext
    zm,rm=np.meshgrid(Ne_ea.xe(1),Ne_ea.xe(0))
    #system.set_N(1e15*np.exp(-(rm**2+zm**2)/(2*1e-4**2)))
    #system.set_N(1e16*np.exp(-(rm**2+zm**2)/(2*1e-5**2)))
    simulation.system.set_N(1e15*np.exp(-(rm**2+zm**2)/(2*3e-5**2)))
#%%#################################################################################################
# Plots
width = 3
pltmarg = 1
for region in simulation.regions:
    height = (width-pltmarg)/2*(region.zmax-region.zmin)/region.rmax+pltmarg
    print(height)
    region.init_plots((width,height),100*(region.id+1))

def time_step(root_region,N_set_in,N_set_use,N_set_out,dt,order):
    """NOTE: original order was: photoionization, advection, rest of ionization, diffusion.
    Now we include photoionization into "ionization".
    The transport processes are combined: advection, then diffusion."""
    if order=='ion_tra':
        root_region.step_ionization(N_set_in,N_set_use,N_set_out,dt)
        root_region.step_advection_then_diffusion(N_set_out,N_set_out,dt)
    elif order=='tra_ion':
        # Note that in this order, the ionization processes occur at a different
        # charge configuration from when the ionizing fields were calculated
        root_region.step_advection_then_diffusion(N_set_in,N_set_out,dt)
        root_region.step_ionization(N_set_out,N_set_use,N_set_out,dt)
    elif order=='adv_ion_dif':
        root_region.step_advection(N_set_in,N_set_out,dt)
        root_region.step_ionization(N_set_out,N_set_use,N_set_out,dt)
        root_region.step_diffusion(N_set_out,N_set_out,dt)
    else:        
        raise Exception('Unknown operation order '+str(order))

#%%
# Start!
kt = 0
#dt = simulation.dtmax_chooser(0)
tmax=1e-7
rk_order = '2' if simulation.second_order else '1'
s = simulation.system
while simulation.t<=tmax:
    # Prepare variables
    print(header('Step 1/'+rk_order+': Qerr =',myfloat(Qerr(s,'phy')),textwidth=40))
    # Update the field
    simulation.E0 = Efun(simulation.t)
    s.recursively_prepare_vars('phy') # triggers all children updates too, calculates subregions
    simulation.update_region_list()
    # Update dtmax (the output interval)
    Emax = 0
    Emin = None
    for region in simulation.regions:
        if Emin is None:
            Emin = np.min(region.E.arr)
        else:
            Emin = min(Emin, np.min(region.E.arr))
        Emax = max(Emax,np.max(region.E.arr))
    for region in simulation.regions:
        print(region.name,': dr x dz required =',region_drdz(region))
    # Plots and saving
    if simulation.t >= simulation.tnext:
        simulation.dtmax = simulation.dtmax_chooser(Emax)
        # To avoid problems with plotting, save the data first
        simulation.save_regions()
        for region in simulation.regions:
            region.save_data()
            drdz = region_drdz(region)
            np.savez(simulation.root + '/dat/' + region.name + '/drdz{:05d}.npz'.format(
                    simulation.kplot),drdz=drdz,t=simulation.t)
        for region in simulation.regions:
            region.plot()
        print(header('E=',myfloat(Emax/1e6),', t=',myfloat(simulation.t),
                    ', k=',kt,', plot ',simulation.kplot,', dtout=',myfloat(simulation.dtmax),
                    color='blue',bold=True,fill='=',print_kw={'sep':''}),flush=True)
        plt.pause(0.1)
        simulation.tnext += simulation.dtmax
        simulation.kplot += 1
    # Calculate dt
    dt = simulation.dtmax
    dt1 = s.calculate_dt()
    if dt1<dt:
        dt = dt1
    # Nicely align
    if simulation.t + dt > simulation.tnext:
        # since t<tnext, this dt>0
        dt = simulation.tnext - simulation.t
        assert dt>0
    print(header('E=',myfloat(Emax/1e6),', t=',myfloat(simulation.t),', dt=',myfloat(dt),', ',
                 myfloat((simulation.tnext-simulation.t)/dt),
                 ' steps to out',color='red',textwidth=80,print_kw={'sep':''}),flush=True)
    # Various processes, choose first or second order
    if simulation.second_order:
        time_step(s,'phy','phy','aux',dt/2,order=simulation.operation_order)
        print(header('Step 2/2: Qerr =',myfloat(Qerr(s,'aux')),textwidth=40))
        simulation.E0 = Efun(simulation.t + dt/2) # if E0 changes, set it here
        s.recursively_prepare_vars('aux')
        time_step(s,'phy','aux','phy',dt,order=simulation.operation_order)
    else:
        time_step(s,'phy','phy','phy',dt,order=simulation.operation_order)
    simulation.t += dt
    kt += 1
    if not computing_environment.HPC_BATCH:
        logfile.flush()
if not computing_environment.HPC_BATCH:
    logfile.close()
