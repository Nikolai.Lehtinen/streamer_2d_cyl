#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Dec  4 19:18:10 2016

Discharge modeling with diffusion and drift velocity, in 2D cylindrical coordinates

Attempt #2, with solution of electron/ion transport instead of current/conductivity concepts.
Previous version: streamer_2D_incorrect_diffusion.py

Uses: photoionization.py, chemistry.py, my_utils.py

Equation to solve:

laplacian phi = - rho
rho = e*(Np-Ne-Nn)
E = -grad phi
dNe/dt+div(v*Ne) = D laplacian Ne + (nui-nua)*Ne + nud*Nn - bete*Ne*Np + Scr + NeS + p
dNp/dt = nui*Ne-bete*Ne*Np + Scr + NeS + p
dNn/dt = nua*Ne-nud*Nn
v = - mu E (electrons)
Scr is cosmic ray source
NeS is initial ionization
p is photoionization, p = Aphot * sum Ck pk
(1-Lamk**2 Laplacian)pk = nui*Ne

Version with a single streamer

@author: nle003
"""

#%% Preliminaries
# Imports
import computing_environment
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import scipy.interpolate
import os
import pickle
import subprocess
#import time
# My packages
import my_utils
from my_utils import phys, myfloat, header #, line_interp2d
from my_utils import get_colors as CC
WARN=CC('red')+'WARNING:'+CC('end')
import trump
from trump import end, span, Grid, ExtendedArray1D, ExtendedArray2D, \
    Field2D, Solver2D, c_ave, n_ave, c_dif
from trump.trump_2D import Geometry2D_cyl, UpdateablePcolorSubplots, AdvectionStepI
from trump.trump_periodic_view import PeriodicViewEA2D,periodic_ea2_interpolate, InfiniteGrid
from trump.trump_saving import ea_to_tuple, tuple_to_ea, same_geom

def n_aver(f):
    g=f.geom
    return n_ave(f*g.rd,0)/g.ri
#def n_aver(f): return n_ave(f,0)
def n_avez(f): return n_ave(f,1)
def c_aver(f):
    g=f.geom
    return c_ave(f*g.ri,0)/g.rd
def c_avez(f): return c_ave(f,1)
def rectangle(rmax,zmin,zmax,scale,zlo=None,zhi=None):
    "Routine for plotting"
    #print('RECTANGLE: rmax=',rmax,', zmin=',zmin,', zmax=',zmax,', zlo=',zlo,', zhi=',zhi)
    if zlo is not None and (zmax>zhi or zmin<zlo):
        # A wrapping rectangle
        zp = zhi-zlo
        if zmax>zhi:
            zmax1 = zmax - zp
            zmin1 = zmin
        elif zmin<zlo:
            # Should never happen
            raise Exception('zmin='+str(zmin))
            zmin1 = zmin + zp
            zmax1 = zmax
        else:
            raise Exception('should not be here')
        assert zmin1>zmax1 # The rectangle is small enough
        bx = np.array([-rmax,-rmax,rmax,rmax,np.nan,-rmax,-rmax,rmax,rmax])/scale
        by = np.array([zhi,zmin1,zmin1,zhi,np.nan,zlo,zmax1,zmax1,zlo])/scale            
    else:
        bx = np.array([-rmax,rmax,rmax,-rmax,-rmax])/scale
        by = np.array([zmin,zmin,zmax,zmax,zmin])/scale
    return bx,by
import chemistry
# Extra operators
def Helmholtz(f,lam2):
    return f-f.geom.Laplacian(f)*lam2
def Diffusion(f,Dcoef):
    g=f.geom
    fr,fz=g.Grad(f)
    return g.Div(fr*c_aver(Dcoef),fz*c_avez(Dcoef))
# A couple of routines for diagnostics
def Qerr(region,N_set='phy'):
    Ne, Np, Nn = (region.Ne.ext, region.Np.ext, region.Nn.ext) if N_set=='phy' else \
        (region.Ne_aux.ext, region.Np_aux.ext, region.Nn_aux.ext)
    return region.geom.integrate(Np-Nn-Ne)
# Matplotlib preferences
mpl.rcParams['font.size'] = 10.0
#mpl.rcParams['figure.titlesize']='medium'
mpl.rcParams['axes.titlesize']='small'

# The root of all ever
rootdir = computing_environment.DATA + 'streamer_2d_cyl/'

#%%#################################################################################################
# A few EA utilities
def ea_liminfo(f):
    return '[{:.3g},{:.3g}]'.format(np.min(f.arr),np.max(f.arr))
def ea_max(f1,f2): return(f1+f2+np.abs(f1-f2))/2.
def ea_min(f1,f2): return(f1+f2-np.abs(f1-f2))/2.
def ea_where(ea):
    i1,i2 = np.where(ea.arr)
    return (i1-ea.nls[0],i2-ea.nls[1])
def ea_interpolate(x,y,ea2,log,kind):
    "Using scipy interp2d convention x(N), y(M) -> array(NxM)"
    if not log=='linear':
        raise Exception('log interpolations are disabled now')
    if isinstance(ea2,PeriodicViewEA2D):
        return periodic_ea2_interpolate(x,y,ea2,kind)
    if log=='log':
        d = np.min(ea2.arr)
        m = np.max(ea2.arr)
        arr = np.log(ea2.arr-d+m/1e10)
    elif log=='linear':
        arr = ea2.arr
    else:
        raise Exception('Must be linear or log')
    #tmp = scipy.interpolate.interp2d(ea2.xe(1), ea2.xe(0), arr,
    #                                  bounds_error=True, kind=kind)(y, x)
    # New version: do not go outside the boundaries
    tmp = scipy.interpolate.interp2d(ea2.x(1), ea2.xe(0), ea2[span,:],
                                      bounds_error=True, kind=kind)(y, x)    
    if log=='log':
        return np.exp(tmp)
    else:
        return tmp
def ea_dispatch(ea_arg,boundaries):
    tmp = ea_arg.copy()
    tmp.arr = my_utils.make_dispatch(ea_arg.arr,boundaries)
    return tmp

#%%#################################################################################################
# Grid size
def required_dxi(ea,a,wi=None):
    if wi is None:
        wi = 0.1
    lap = np.abs(trump.n_dif(trump.c_dif(ea,a),a))
    absea = np.abs(ea)
    dxi = np.sqrt(lap/absea)
    dxi.arr[~np.isfinite(dxi.arr)]=0
    maxea = np.max(absea[:,:])
    #tmp0 = dxi*(absea/maxea)**wi
    #tmp0.arr[np.isnan(tmp0.arr)]=0
    #tmp1 = dxi*(absea>maxea*1e-1)
    #assert not np.isnan(tmp1.arr).any()
    tmp2 = dxi*(absea>maxea*1e-2) # <- I think we should use this
    assert not np.isnan(tmp2.arr).any()
    #tmp3 = dxi*(absea>maxea*1e-3)
    #assert not np.isnan(tmp3.arr).any()
    #return [tmp0,tmp1,tmp2,tmp3]
    return tmp2
def required_dxi_in_region(r,a,wi=None):
    reactions = [r.Ne.ext, r.nui*r.Ne.ext, r.Ne.ext*r.Np.ext, r.nua*r.Ne.ext]
    dxi = required_dxi(reactions[0],a)
    for r in reactions[1:]:
        #tmp = required_dxi(r,a)
        #for k in range(4):
        #    dxi[k] = ea_max(dxi[k], tmp[k])
        dxi = ea_max(dxi,required_dxi(r,a))
    return dxi
def region_drdz(r):
    dri = required_dxi_in_region(r,0)
    #dr = np.array([1/np.max(dri[k][:,:]) for k in range(4)])
    dr = 1/np.max(dri[:,:])
    dzi = required_dxi_in_region(r,1)
    #dz = np.array([1/np.max(dzi[k][:,:]) for k in range(4)])
    dz = 1/np.max(dzi[:,:])
    return np.array([dr,dz]).T

#%%#################################################################################################
# The boundary condition setups or simple applications
# Conventions:
# The border is given as a pair (start, extra) where both start, extra>=0 and are counted from the
# internal area. By the term "internal" we mean the area of independent variables for global fields
# or the area used for updating the parent region for non-global fields (which we will call
# "local"). This internal area is
#     [0:end-1,0:end-1] for the global region
#     [0:end-1,1:end-1] for local regions
# This may be generalized if we use (nlz = 1 if is_global else 0) to [0:end-1,1-nlz:end-1]
# Thus, the array sizes are
#     nls=(extra,extra-1+nlz), nus=(extra-1,extra-1)
# extra == 0 corresponds to the internal area. It should be used for variables which are never
# differentiated or averaged, e.g., Np and Nn.
# For solution of Laplace and similar equations we need extra==1
# For 3-rd order advected variables (e.g., Ne) we need extra==2. If E is stag=(0,0) and is used
# to calculate Ne, then for phi we need extra == 3.
# Variable "start" must be >0 when we set non-null boundary conditions.
#
# In these function ,we record BC if "symbolic" is True, otherwise apply BC to an ExtendedArray2D.
def bc_periodic(symbolic,field,border):
    "Works on arbitrary r-geometry. But in the streamer simulation, is only used for global fields."
    # Independent layers ar nz=0..end-1
    start,extra = border
    assert start>0 # cannot be smaller
    assert extra>=start # otherwise, nothing to do. Then also extra>=1 automatically
    ea = field.bc if symbolic else field
    assert field.stags[1]==0 and field.nls[1]==extra and field.nus[1]==extra-1
    # -- or could calculate extra from field.
    if symbolic: ea.record('periodic bottom->top',is_variable=False)
    ea[span,end-1+start:end-1+extra] = ea[span,start-1:extra-1]
    if symbolic: ea.record('periodic top->bottom',is_variable=False)
    ea[span,-extra:-start] = ea[span,end-extra:end-start]
def bc_axis(symbolic,field,border,zslice):
    "Works on arbitrary z-geometry. The slice in z where BC is applied is given."
    start,extra = border
    assert start>0
    assert extra>=start
    ea = field.bc if symbolic else field
    assert field.stags[0]==0 and field.nls[0]==extra
    if symbolic: ea.record('axis',is_variable=False)
    ea[-start:-extra:-1,zslice] = ea[start:extra,zslice]
def bc_outer(symbolic,field,border,bctype,zslice):
    "Works on arbitrary z-geometry. The slice in z where BC is applied is given."
    start,extra = border
    assert start>0
    assert extra>=start
    assert field.stags[0]==0 and field.nus[0]==extra-1
    ea = field.bc if symbolic else field
    if bctype=='Neumann':
        if symbolic: ea.record('outer boundary (Neumann)',is_variable=False)
        for k in range(start-1,extra):
            # No way to avoid cycle because the rhs is the same
            ea[end+k,zslice]=ea[end-1,zslice]
    elif bctype=='Dirichlet':
        if symbolic: ea.record('outer boundary (Dirichlet)',is_variable=False)
        ea[end-1+start:end-1+extra,zslice]=0
    else:
        raise Exception('Unknown BC for outer boundary: '+str(bctype))
def bc_from_another(symbolic,field,border,fanother,setup,interp_opts):
    """interp_opts can be, e.g, ('linear','linear').
    This function only applies to local fields!"""
    start,extra = border
    assert start>0
    assert extra>=start
    ea = field.bc if symbolic else field
    #print(field.nls,field.nus,extra)
    assert field.nls[1]==extra-1 and field.nus==(extra-1,extra-1)
    outer = (slice(end-1+start,end-1+extra),slice(-extra+1,end-1+extra)) # or outer[1]==span
    upper = (slice(0,end-2+start),slice(end-1+start,end-1+extra))
    lower = (slice(0,end-2+start),slice(-extra+1,-start+1))
    for boundary,boundary_name in [(outer,'outer'),(upper,'upper'),(lower,'lower')]:
        if trump.DEBUGLEVEL>3:
            if not symbolic:
                m = 'Setting'
            else:
                m = 'Recording' if setup else 'Updating'
            print(m,boundary_name,'boundary',boundary,'of',field)
        bc_name = boundary_name+' boundary from parent'
        if symbolic:
            if setup:
                ea.record(bc_name,is_variable=True)
            else:
                ea.update(bc_name)
        r,z=field.geom.xs
        log,kind = interp_opts
        rhs = ea_interpolate(r[boundary[0]],z[boundary[1]],fanother,log,kind)
        if symbolic: rhs = rhs.flatten()
        ea[boundary] = rhs
        if symbolic: ea.end_update()

def bc_record_or_simple_apply(symbolic,field,extra,outer_bc,gauge,fanother,interp_opts):
    "Record (if symbolic) or apply (if not symbolic) BC"
    assert extra>0
    ea = field.bc if symbolic else field
    close_border = (1,1) if symbolic else (1,extra)
    is_global = (fanother is None)
    if is_global:
        bc_periodic(symbolic,field,close_border)
        bc_axis(symbolic,field,close_border,slice(0,end-1))
        bc_outer(symbolic,field,close_border,outer_bc,slice(0,end-1))
        if symbolic and gauge is not None: # Do not apply to non-symbolic arrays
            for k,gc in enumerate(gauge):
                if symbolic: ea.record('gauge condition #'+str(k),is_variable=False)
                ea[gc[0]]=gc[1]
    else:
        assert gauge is None # cannot have both
        far_border = (extra,extra) if symbolic else (1,extra)
        setup = symbolic # Ha!
        bc_from_another(symbolic,field,far_border,fanother,setup,interp_opts) 
        bc_axis(symbolic,field,close_border,span)
    if symbolic: ea.freeze()

#%%#################################################################################################
class RegionField:
    """A field representation just for the cylindrical multi-region calculation.
    The field values are stored in "extended" self.ext (which includes all the BC), but for
    solving PDE the "solveable" sub-array self.sol is used. The size of self.sol
    depends on whether the region is global or local"""
    def __init__(self,name,region,extra,derived,outer_bc=None,gauge=None):
        # Register with the region to make parent and childen aware
        if name in region.env:
            raise Exception('Field',name,'already exists')
        region.env[name] = self
        if region.is_global:
            self.parent = None
        else:
            self.parent = region.parent.env[name]
        self.name = name
        self.region = region
        self.extra = extra
        self.derived = derived
        self.is_global = region.is_global
        self.outer_bc = outer_bc
        self.gauge = gauge
        #nlz = 1 if self.is_global else 0
        # Extended and independed areas
        if self.is_global:
            self.ext = ExtendedArray2D(region.geom,stags=(0,0),nls=(extra,extra),
                                         nus=(extra-1,extra-1))
        else:
            self.ext = ExtendedArray2D(region.geom,stags=(0,0),nls=(extra,extra-1),
                                         nus=(extra-1,extra-1))    
        self.ext.alloc(np.double)
        if extra>0:
            # Field which will be needed for solving equations
            if self.is_global:
                self.sol = Field2D(name,region.geom,stags=(0,0),nls=(1,1),nus=(0,0),
                             use_array=self.ext[-1:end,-1:end])
                fanother = None
            else:
                self.sol = Field2D(name,region.geom,stags=(0,0),
                                   nls=(1,extra-1),nus=(extra-1,extra-1),
                                   use_array=self.ext[-1:end-1+extra,span])
                fanother = PeriodicViewEA2D(self.parent.ext,axis=1) \
                    if self.parent.is_global else self.parent.ext
            # Record the symbolic description of BC for the solveable
            self.interp_opts = ('linear','cubic') if self.derived else ('linear','linear')
            bc_record_or_simple_apply(True,self.sol,extra,outer_bc,gauge,fanother,self.interp_opts)
        # In principle, we should also interpolate the internal from parent when the field is created
        if not self.derived and not self.is_global:
            self.set_value(None)
        # See set_value
    def couple_sol_to_ext(self):
        """Couple the solveable to the sub-array of extended. Needed after restoring the pickled
        version"""
        if self.extra>0:
            if self.is_global:
                self.sol.arr = self.ext[-1:end,-1:end]
            else:
                self.sol.arr = self.ext[-1:end-1+self.extra,span]
    def update_bc(self):
        """Update symbolic bc for solving equations - use just apply_bc if this field is never
        an unknown in an equation"""
        if not self.is_global and self.extra>0:
            self.interp_opts = ('linear','cubic') if self.derived else ('linear','linear')
            proxy = PeriodicViewEA2D(self.parent.ext,axis=1) \
                if self.parent.is_global else self.parent.ext
            bc_from_another(True,self.sol,(self.extra,self.extra), proxy, False, self.interp_opts)
    def apply_bc(self):
        # Disregard self.sol.bc.apply()
        if self.extra>0:
            if self.is_global:
                fanother = None
            else:
                fanother = PeriodicViewEA2D(self.parent.ext,axis=1) \
                    if self.parent.is_global else self.parent.ext
            self.interp_opts = ('linear','cubic') if self.derived else ('linear','linear')
            bc_record_or_simple_apply(False,self.ext,self.extra,self.outer_bc,self.gauge,
                                  fanother,self.interp_opts)
    def update_parent(self):
        assert not self.derived
        if not self.is_global:
            r = self.region
            # We might need to wrap around
            proxy = PeriodicViewEA2D(self.parent.ext,axis=1) \
                if self.parent.is_global else self.parent.ext
            f = r.fringe
            proxy[0:r.irmax-f,r.izmin+f:r.izmax-f] = r.geom.integrate(self.ext,r.rb,r.zb)/r.volume
    def set_value(self,arr):
        self.update_bc()
        if self.is_global:
            assert arr is not None
            self.ext.arr[...]=arr[...]
        else:
            assert arr is None
            r0,z0=self.region.geom.xs
            e=self.extra
            r=r0[-e:end+e-1] # maybe we should not go to negative radii
            #r=r0[0:end+e-1]
            z=z0[-e+1:end+e-1]
            # Since this is only Ne,Nn,Np, we use log interpolation
            proxy = PeriodicViewEA2D(self.parent.ext,axis=1) \
                if self.parent.is_global else self.parent.ext
            self.ext.arr[...] = ea_interpolate(r,z,proxy,'linear','linear')
        self.apply_bc() # - overrides the negative radii interpolation anyway
    def solver(self,equation,skip=None):
        return Solver2D(self.sol,equation(self.sol.symb),skip)
    def solve(self,solver,rhs):
        """Solve a 2nd order PDE using 'solver'. The size of RHS is fixed, it is the solvable's
        size minus one at each boundary."""
        if self.is_global:
            solver.solve_full(self.sol,rhs.view[:end-1,:end-1])
        else:
            e = self.extra
            solver.solve_full(self.sol,rhs.view[:end+e-2,-e+2:end+e-2])
        # After this, we still need to apply BC to fill out the boundary of self.ext
    def __repr__(self):
        return 'RegionField(derived='+str(self.derived)+', ext='+str(self.ext)+\
            ', sol='+str(self.sol)+')'

#%%#################################################################################################
class PhotoIonization:
    def __init__(self,region):
        #self.nlz = region.nlz
        sim = region.simulation
        self.select_p=sim.select_p
        normphot = np.sum(sim.Cphot[self.select_p])
        self.Aphotnorm = sim.Aphot*sim.Cphot/normphot
        #self.is_global = region.is_global
        self.ptot=None
        self.Nterms = len(sim.Cphot)
        assert len(sim.Lphot)==self.Nterms
        self.p=[None]*self.Nterms
        self.p_solver=[None]*self.Nterms
        for k in self.select_p:
            name = 'p'+str(k+1)
            extra = region.simulation.adv_extra
            p_ = RegionField(name,region,extra,True,'Dirichlet')
            if self.ptot is None:
                self.ptot = p_.ext.copy()
                self.ptot.alloc(np.double)
            self.p[k]=p_
            self.p_solver[k]=self.p[k].solver(lambda p: Helmholtz(p,sim.Lphot[k]**2))
    def solve(self,Si):
        self.ptot.arr[...]=0
        for k in self.select_p:
            #if trump.DEBUGLEVEL>2: print(CC('red')+'SOLVING FOR P[',k,']'+CC('end'))
            self.p[k].solve(self.p_solver[k],Si)
            self.ptot.arr[...] += self.Aphotnorm[k]*self.p[k].ext.arr[...]

####################################################################################################
#%% A simulation region with variables in it
def check_unique_num(rs):
    nums = np.unique([r.num for r in rs])
    assert len(nums)==len(rs)
    assert all([int(r.name.split('_')[-1])==r.num for r in rs])        
def get_available_num(rs):
    check_unique_num(rs)
    nums = np.unique([r.num for r in rs])
    num = 0
    while True:
        if num in nums:
            num += 1
            continue
        else:
            break
    return num
class Region:
    """
    General strategy: each step for modification of fields starts with fields with valid BC.
    Thus, BC in child regions are determined by parent region values BEFORE the step.
    The BC are updated in the END of step.
    The time step schema:
        # Do the actual stepping
        The parent makes a step
        Each child:
            Make step
                ... recursive call to grandchildren ...
            A. Send the calculated interior to update parent
            B. Update and apply bc from parent info
        A. The parent sends interior to its parent
        B. Updates and applies its BC
    Items A and B are independent on the order and may be switched.
    
    The preparation of variables has a different schema. Here, no time is involved,
    e.g. updating phi and photoionization from N. Here N is independent variable, while
    phi and photoionization components are derived variables.
    We start with valid independent variables with valid BC.
        The parent calculates derived var
        The BC are applied to derived var (maybe simultaneously with previous step)
        At this point, we should try to detect new subregions
        Each child:
            Updates BC for derived var from parent
            Calculates derived var + apply BC
            ... recursive call to grandchildren ...
    
    New version: do not update parent child list or simulation region list in the constructor!
    """
    def __init__(self,region_id,simulation,name,parent,rmax,zmin,zmax,dr,dz):
        parent_name='' if parent is None else parent.name
        #self.creation_info={'name':name,'parent_name':parent_name,
        #                    'rmax':rmax,'zmin':zmin,'zmax':zmax,'dr':dr,'dz':dz}
        self.id = region_id #simulation.next_region_id #self.__class__.id_counter
        #simulation.next_region_id += 1 #self.__class__.id_counter += 1
        if not (rmax>0 and zmax>zmin):
            print('rmax=',rmax,', zmin=',zmin,', zmax=',zmax)
            raise Exception('invalid parameters')
        self.creation_info=[region_id,name,parent_name,rmax,zmin,zmax,dr,dz]
        self.simulation = simulation # will be same for all regions
        self.fringe = simulation.fringe
        self.interp_margin = simulation.interp_margin
        self.figinitstage = 0
        #simulation.regions.append(self)
        self.name = name
        self.parent = parent # The parent region
        self.env = {} # registry for global fields
        self.children = []
        self.is_global = (parent is None)
        self.all_plots = simulation.all_plots or (self.is_global and simulation.all_global_plots)
        self.depth = 0 if self.is_global else parent.depth+1
        #self.nlz = 1 if self.is_global else 0 # extra points at lower z boundary
        self.figs=[]
        Nr_approx,Nz_approx=rmax/dr,(zmax-zmin)/dz
        Nr_fix = np.int(np.rint(Nr_approx)); Nz_fix = np.int(np.rint(Nz_approx))
        round_error = simulation.round_error
        if np.abs(Nr_fix-Nr_approx)>round_error or np.abs(Nz_fix-Nz_approx)>round_error:
            print('Nr_approx=',Nr_approx,', Nz_approx=',Nz_approx)
            raise Exception('Please choose dr,dz so that there is a whole number of grid points')
        if self.is_global:
            Nr = Nr_fix; Nz = Nz_fix
            self.zgrid_period = Nz
        else:
            self.zgrid_period = None
            #parent.children.append(self)
            # Calculate the indices of the boundary into the parent region
            dr_par = parent.dr
            dz_par = parent.dz
            zmin_par = parent.zmin
            # Coordinates inside the parent
            irmax_approx = rmax/dr_par
            izmin_approx = (zmin-zmin_par)/dz_par
            izmax_approx = (zmax-zmin_par)/dz_par
            self.irmax = np.int(np.rint(irmax_approx))
            self.izmin = np.int(np.rint(izmin_approx))
            self.izmax = np.int(np.rint(izmax_approx))
            if np.abs(self.irmax-irmax_approx)>round_error or \
                    np.abs(self.izmin-izmin_approx)>round_error or \
                    np.abs(self.izmax-izmax_approx)>round_error:
                print('irmax_approx=',irmax_approx,', izmin_approx=',izmin_approx,
                      ', izmax_approx=',izmax_approx)
                raise Exception('Pease choose dr,dz so that there is a whole number of grid points')
            refiner_approx = dr_par/dr
            refinez_approx = dz_par/dz
            refiner = np.int(np.rint(refiner_approx))
            refinez = np.int(np.rint(refiner_approx))
            if np.abs(refiner_approx-refiner)>round_error or \
                    np.abs(refinez_approx-refinez)>round_error:
                print('refiner_approx=',refiner_approx,', refinez_approx=',refinez_approx)
                raise Exception('Pease choose dr,dz so that there is a whole number of grid points')
            if simulation.adaptive:
                assert simulation.refine[0]==refiner and simulation.refine[1]==refinez
            Nr=refiner*self.irmax
            Nz=refinez*(self.izmax-self.izmin)
            assert Nr==Nr_fix and Nz==Nz_fix
            dr=dr_par/refiner
            dz=dz_par/refinez
            #z_par = InfiniteGrid(parent.geom.gridz,0) if parent.is_global else parent.geom.xs[1]
            z_par = parent.geom.xs[1]
            zmin = z_par[self.izmin]
            self.prepare_parent_updates()
        print('Creating region ',self.short_info(),' of size ',Nr,'x',Nz,
              ', d=(',myfloat(dr),'x',myfloat(dz),')',
              ', r=[0, ',myfloat(rmax),'], z=[',myfloat(zmin),', ',myfloat(zmax),']',
              sep='',flush=True)
        gridr = Grid(n_cells=Nr, delta=dr, start=0)
        gridz = Grid(n_cells=Nz, delta=dz, start=zmin)
        self.geom = Geometry2D_cyl(gridr,gridz,nls=simulation.nls,nus=simulation.nus)
        # A fix for the periodicity
        #if self.is_global:
        #    self.geom.xs = (self.geom.xs[0],InfiniteGrid(self.geom.gridz,0))
        #    self.geom.xcs = (self.geom.xcs[0],InfiniteGrid(self.geom.gridz,1))
        self.is_grandchild = ExtendedArray2D(self.geom,(0,0),nls=(0,0),nus=(0,0))
        self.is_grandchild.alloc(np.bool)
        # Fix value phi=0 at a point with coordinates [Nr-5,5]
        gauge = [((Nr-5,5),0)] if self.is_global else None
        extra = simulation.adv_extra
        if simulation.do_photoionization:
            self.photoionization = PhotoIonization(self)
        self.phi = RegionField('phi',self,extra+1,True,'Neumann',gauge)
        skip = self.phi.sol.dep.view[0:end-1,0:end-1] if self.is_global else None
        self.phi_solver = self.phi.solver(lambda phi: -phys.epsilon_0*self.geom.Laplacian(phi),skip)
        self.Ne = RegionField('Ne',self,extra,False,'Neumann')
        # Although the following are not differentiated, the extended values are needed for
        # calculation of phi.
        self.Np = RegionField('Np',self,extra,False,'Neumann')
        self.Nn = RegionField('Nn',self,extra,False,'Neumann')
        # More memory but less mess
        self.Ne_tmp = self.Ne.ext.copy()
        self.Ne_tmp.alloc(np.double)
        self.Np_tmp = self.Np.ext.copy()
        self.Np_tmp.alloc(np.double)
        self.Nn_tmp = self.Nn.ext.copy()
        self.Nn_tmp.alloc(np.double)
        self.dN = self.Ne.ext.copy()
        self.dN.alloc(np.double)
        if simulation.second_order:
            self.Ne_aux = RegionField('Ne_aux',self,extra,False,'Neumann')
            self.Np_aux = RegionField('Np_aux',self,extra,False,'Neumann')
            self.Nn_aux = RegionField('Nn_aux',self,extra,False,'Neumann')
        else:
            # These are not used
            self.Ne_aux = None
            self.Np_aux = None
            self.Nn_aux = None
        self.first_time = True
        self.calculated_derived_fields = False
    def short_info(self):
        return self.name+'(id='+str(self.id)+')'
    @property
    def dr(self):
        return self.geom.gridr.delta
    @property
    def dz(self):
        return self.geom.gridz.delta
    @property
    def rmax(self):
        return self.geom.xs[0][end]
    @property
    def zmin(self):
        return self.geom.gridz.start
    @property
    def zmax(self):
        return self.geom.xs[1][end]
    @property
    def Nr(self):
        return self.geom.gridr.n_cells
    @property
    def Nz(self):
        return self.geom.gridz.n_cells
    def prepare_parent_updates(self):
        "Internal info to optimize sending the interior to the parent"
        rcp,zcp = self.parent.geom.xcs
        if self.parent.is_global:
            zcp = InfiniteGrid(self.parent.geom.gridz,1)
        # Was:
        #rb = np.hstack((0,rcp[0:self.irmax-1],rp[self.irmax]))
        #zb = np.hstack((zp[self.izmin],zcp[self.izmin:self.izmax-1],zp[self.izmax]))
        #Take only the middle
        f = self.fringe
        self.rb = np.hstack((0,rcp[0:self.irmax-f]))
        izmax = self.izmax
        if izmax<self.izmin:
            assert self.parent.is_global
            izmax += self.parent.Nz
        self.zb = zcp[self.izmin+f-1:izmax-f]
        self.volume = np.pi*np.expand_dims(np.diff(self.rb**2),axis=1) * \
            np.expand_dims(np.diff(self.zb),axis=0)
    def shift_geometry_in_z(self,zshift):
        "Manipulate self.geom so that z is shifted by zshift"
        gz = self.geom.gridz
        gridz = Grid(n_cells=gz.n_cells, delta=gz.delta, start=gz.start + zshift)
        self.geom.grids = (self.geom.grids[0],gridz) # cannot replace in a tuple
        self.geom.gridr = self.geom.grids[0]
        self.geom.gridz = self.geom.grids[1]
        self.geom.calculate_xs(nls=self.simulation.nls,nus=self.simulation.nus)
        self.creation_info[4] += zshift
        self.creation_info[5] += zshift
        self.prepare_parent_updates() # this must be called with valid parent geometry!
    def recursively_shift_in_z(self,zshift):
        self.shift_geometry_in_z(zshift)
        for r in self.children:
            r.recursively_shift_in_z(zshift)
    def move(self,newizmin):
        """The policy for moving "self" inside a periodic domain "self.parent" is the following:
            1. self.izmin (which shows the position of self inside self.parent) is always positive
            2. self.izmax can exceed self.parent.Nz
        The same applies to self.zmin, self.zmax: we can have self.zmax>self.parent.zmax but not
        self.zmin<self.parent.zmin.
        Namely, the subdomain may stick out from above, but not from below"""
        assert not self.is_global
        assert self.simulation.adaptive
        izsize = self.izmax-self.izmin
        izmove = newizmin - self.izmin
        # Avoid the big jump across the whole periodic region
        # This is important because "izmove" is used later to move the data inside "self"
        if self.parent.is_global:
            p = self.parent.Nz
            izmove = ((izmove+p//2) % p) - p//2
        self.izmin = self.izmin + izmove
        switch=0
        if self.parent.is_global:
            switch = self.izmin // p
            self.izmin = self.izmin % p # same as self.izmin - switch*p
        self.izmax = self.izmin + izsize
        # z_par will need axess outside the grid for a periodic domain
        z_par = InfiniteGrid(self.parent.geom.gridz,0) \
            if self.parent.is_global else self.parent.geom.xs[1]
        # Also could have made a periodic proxy of self.parent.geom.xs[1]
        #z_par = self.parent.geom.xs[1]
        zmin = z_par[self.izmin]
        zmax = z_par[self.izmax]
        assert zmax>zmin
        # -- according to the policy (see comment), zmin > self.parent.zmin but we may have
        # zmax > self.parent.zmax
        #if zmax<zmin:
        #    assert self.parent.is_global
        #    zmax += p*self.parent.dz
        gz = self.geom.gridz
        refinez_approx = self.parent.dz/self.dz
        refinez = np.int(np.rint(refinez_approx))
        rerr = self.simulation.round_error
        assert self.simulation.refine[1]==refinez and np.abs(refinez_approx-refinez)<rerr
        izmoveself = izmove*refinez
        #izmoveself = np.int(np.round(izmove*self.parent.dz/self.dz))
        print('Moving',self.name,'by',izmove,'(',izmoveself,') to start=',
              zmin,'=',gz.start+izmove*self.parent.dz)
        gridz = Grid(n_cells=gz.n_cells, delta=gz.delta, start=zmin)
        # -- cannot replace in a tuple
        # Monkey-patch the geometry so that the object still stays the same
        # Cannot set self.geom because there are other objects pointing to it!
        self.geom.grids=(self.geom.grids[0],gridz) # cannot replace in a tuple
        # Do the same stuff when we set up the geometry
        self.geom.gridr=self.geom.grids[0]
        self.geom.gridz=self.geom.grids[1]
        self.geom.calculate_xs(nls=self.simulation.nls,nus=self.simulation.nus)
        self.creation_info[4]=zmin
        self.creation_info[5]=zmax
        if not (np.abs((self.zmin-zmin)/self.dz)<rerr and np.abs((self.zmax-zmax)/self.dz)<rerr):
            print('zmin=',zmin,'; self.zmin=',self.zmin,'; zmax=',zmax,'; self.zmax=',self.zmax)
        assert np.abs((self.zmin-zmin)/self.dz)<rerr and np.abs((self.zmax-zmax)/self.dz)<rerr
        # Move all subregions
        for region in self.children:
            region.izmin -= izmoveself
            region.izmax -= izmoveself
        # ri and rd stay the same
        self.prepare_parent_updates()
        # Shift the field values that are still inside this region
        for fname in self.env: # Only non-derived fields
            # Try interpolating from
            rfield = self.env[fname]
            if rfield.derived: continue
            rfield.update_bc()
            tmp = rfield.ext.copy()
            tmp.arr = rfield.ext.arr.copy()
            # Interpolate from parent
            rfield.set_value(None)
            if izmoveself>0:
                rfield.ext[:,0:end-izmoveself]=tmp[:,izmoveself:end]
            else:
                rfield.ext[:,-izmoveself:end]=tmp[:,0:end+izmoveself]
        # Update the axes on the plots
        for fig in self.figs:
            if fig is not None:
                for uax in fig.uaxs:
                    uax.update_axes = True
        if switch!=0:
            Lparent = self.parent.zmax-self.parent.zmin # same as p*self.parent.dz
            zswitch = -switch*Lparent
            print('All children of',self.short_info(),'are shifted in z by',zswitch)
            # Update the children's grids, now that "self" has a valid geometry
            for r in self.children:
                r.recursively_shift_in_z(zswitch)
    def set_figdirs(self):
        if self.simulation.save_fig:
            regiondir = '/fig/' + self.name + '/'
            figdir_N = regiondir + 'N'
            figdir_E = regiondir + 'E'
            figdir_dN = regiondir + 'dN'
            self.simulation.makedir(figdir_N)
            self.simulation.makedir(figdir_E)
            self.simulation.makedir(figdir_dN)
            figdir_N = self.simulation.root + figdir_N
            figdir_E = self.simulation.root + figdir_E
            figdir_dN = self.simulation.root + figdir_dN
        else:
            figdir_N = None
            figdir_E = None
            figdir_dN = None
        self.figdirs = (figdir_N,figdir_E,figdir_dN)
    def init_plots(self,fig1size,startnum):
        "delayed until actual plotting"
        self.fig1size = fig1size
        self.figstartnum = startnum
        self.figinitstage = 1
    def init_plots_real(self,fig1size,startnum):
        savefmts = self.simulation.figsavefmts
        cmap = self.simulation.figcmap
        scale = self.simulation.figscale
        FWID,FLEN=fig1size
        print('Init plots #',startnum,', each figsize=',fig1size)
        if self.simulation.adaptive:
            fig1=UpdateablePcolorSubplots(1,4,num=startnum+1,figsize=(4*FWID,FLEN),
                pseudos=[False]*4,scale=scale,cmap=cmap,
                savedir=None,savefmts=savefmts)
            # Monkey-patch non-defaults
            uax = fig1.uaxs[3]
            uax.cmap = mpl.cm.viridis
            uax.zmin0 = 0
            uax.zmax0 = len(self.simulation.badness_boundaries)+1
        else:
            fig1=UpdateablePcolorSubplots(1,3,num=startnum+1,figsize=(3*FWID,FLEN),
                pseudos=[False]*3,scale=scale,cmap=cmap,
                savedir=None,savefmts=savefmts)
        if self.all_plots:
            fig2=UpdateablePcolorSubplots(1,3,num=startnum+2,figsize=(3*FWID,FLEN),
                pseudos=[True,False,False],scale=scale,cmap=cmap,
                savedir=None,savefmts=savefmts)
            fig3=UpdateablePcolorSubplots(1,5,num=startnum+3,figsize=(5*FWID,FLEN),
                pseudos=[False]*5,scale=scale,cmap=cmap,
                savedir=None,savefmts=savefmts)
        else:
            fig2 = None
            fig3 = None
        self.figs = [fig1,fig2,fig3]
        self.subregion_contours = []
    def plot_subregion_contours(self):
        # Erase the old contours
        for line in self.subregion_contours:
            line.remove()
        # Plot the new ones
        self.subregion_contours = []
        zlo = self.zmin if self.is_global else None
        zhi = self.zmax if self.is_global else None
        for subregion in self.children:
            bx,by = rectangle(subregion.rmax,subregion.zmin,subregion.zmax,
                              self.simulation.figscale,zlo=zlo,zhi=zhi)
            for fig in self.figs:
                if fig is not None:
                    for uax in fig.uaxs:
                        line,=uax.ax.plot(bx,by,'w',linewidth=.5)
                        self.subregion_contours.append(line)
    def plot(self):
        #global fig1,fig2,fig3,t,Ercr,Ezcz,E,Ne_tmp,Ne,Np,Nn,Si,nua,nud,ptot
        assert self.figinitstage!=0
        # Values of figinitstage are:
        # 0: just after region creation, 1: after plot init, -1: after plot close, 2: plotted before
        if self.figinitstage==1:
            # Not initialized yet
            self.init_plots_real(self.fig1size,self.figstartnum)
            self.figinitstage=2
        elif self.figinitstage==-1:
            self.close_real()
            return
        Ne,Np,Nn = self.Ne,self.Np,self.Nn
        t = self.simulation.t
        #print(self.name,'*** t=',myfloat(t),sep='',flush=True)
        fig1,fig2,fig3 = self.figs
        #Ne_tmp.setv = Ne-np.max(Ne.arr)*Ne_source
        fig1.subplot(1,np.log10(Ne.ext+1e10),'logNe:t='+str(myfloat(t))+','+ea_liminfo(Ne.ext))
        fig1.subplot(2,np.log10(Np.ext+1e10),'logNp:id='+str(self.id)+','+ea_liminfo(Np.ext))
        fig1.subplot(3,np.log10(Nn.ext+1e10),'logNn:'+self.name+','+ea_liminfo(Nn.ext))
        if self.simulation.adaptive:
            b=ea_dispatch(self.badness,self.simulation.badness_boundaries)
            # The grandchild info might be stale though, recalculate
            self.locate_grandchildren()
            b[self.is_grandchild] = len(self.simulation.badness_boundaries) + 1
            fig1.subplot(4,b,str(self.simulation.kplot)+': badness')
        if self.all_plots:
            Ercr,Ezcz,E,nua,nud =\
                self.Ercr,self.Ezcz,self.E,self.nua,self.nud
            fig2.subplot(1,Ercr,'Er:t='+str(myfloat(t))+','+ea_liminfo(Ercr))
            fig2.subplot(2,Ezcz,'Ez:'+ea_liminfo(Ezcz))
            fig2.subplot(3,E,'E:'+ea_liminfo(E))
            fig3.subplot(1,self.Si,'Si:t='+str(myfloat(t))+','+ea_liminfo(self.Si))
            fig3.subplot(2,nua*Ne.ext,'nua*Ne:'+ea_liminfo(nua*Ne.ext))
            fig3.subplot(3,nud*Nn.ext,'nud*Nn:'+ea_liminfo(nud*Nn.ext))
            recomb = chemistry.bete*Ne.ext*Np.ext
            fig3.subplot(4,recomb,'recomb:'+ea_liminfo(recomb))
            p = self.photoionization.ptot
            fig3.subplot(5,p,'phot:'+ea_liminfo(p))
        self.plot_subregion_contours()
        self.save_figures()
    def save_figures(self):
        self.set_figdirs()
        for fig,figdir in zip(self.figs,self.figdirs):
            if fig is not None:
                fig.savedir = figdir
                fig.info()
        fig1,fig2,fig3 = self.figs
        fig1.save(self.simulation.kplot)
        if self.all_plots:
            fig2.save(self.simulation.kplot)
            fig3.save(self.simulation.kplot)
    def set_N(self,Ne_arr):
        self.Ne.set_value(Ne_arr)
        # Np -- opposite but equal (Jim Crow condition)
        self.Np.ext.setv = self.Ne.ext
        self.Np.apply_bc()
        # All other fields are presumed zero and to have correct bc
        #self.Nn.arr[...]=0
        #self.Nn.bc.apply()
        for child in self.children:
            child.set_N(Ne_arr)
    def save_data(self):
        if self.simulation.save_dat:
            regiondir =  '/dat/' + self.name + '/'
            self.simulation.makedir(regiondir)
            datadir = self.simulation.root + regiondir
            d={'t':self.simulation.t, 'kplot':self.simulation.kplot}
            #to_save = ['Ne','Np','Nn','phi']
            geom_common = None
            for name,field in self.env.items():
                geom,info,arr = ea_to_tuple(field.ext)
                if geom_common is None:
                    geom_common = geom
                    d.update({'geom':geom})
                assert same_geom(geom,geom_common)
                d.update({name+'_info':info,name+'_arr':arr})
            np.savez_compressed(datadir+'/res{:05d}.npz'.format(self.simulation.kplot),**d)
    def prepare_vars(self,N_set):
        """Preliminary calculations before time stepping:
            phi, E, mu, v, D, phot, nui, nua, nud, Si
        We pre-allocate all the intermediate arrays to avoid creation costs.
        """
        # N does not change, so can just point to "ext" part
        if N_set=='phy':
            Ne, Np, Nn = self.Ne.ext, self.Np.ext, self.Nn.ext
        elif N_set=='aux':
            Ne, Np, Nn = self.Ne_aux.ext, self.Np_aux.ext, self.Nn_aux.ext
        for fname in self.env: # only derived variables
            rfield = self.env[fname]
            if rfield.derived: rfield.update_bc()
        if self.first_time:
            self.rho = phys.e*(Np - Nn - Ne)
        else:
            self.rho.setv = phys.e*(Np - Nn - Ne)
        self.phi.solve(self.phi_solver,self.rho)
        self.phi.apply_bc()
        # Ercr etc are not RegionFields
        if self.first_time:
            self.Ercr = -c_dif(self.phi.ext,0) # at CX-points
            self.Ezcz = -c_dif(self.phi.ext,1)+self.simulation.E0 # at CY-points
            self.E = np.sqrt(n_aver(self.Ercr)**2+n_avez(self.Ezcz)**2)
            self.mue = chemistry.mue_fun(self.E) # needed later for dt calculations
            self.vrcr = -c_aver(self.mue)*self.Ercr
            self.vzcz = -c_avez(self.mue)*self.Ezcz
            self.nui = chemistry.nui_fun(self.E)
            self.nua = chemistry.nua_fun(self.E)
            self.nud = chemistry.nud_fun(self.E)
            self.Si = self.nui*Ne
            if self.simulation.do_diffusion:
                self.D = chemistry.D_fun(self.E)
            self.first_time = False
        else:
            self.Ercr.setv = -c_dif(self.phi.ext,0) # at CX-points
            self.Ezcz.setv = -c_dif(self.phi.ext,1)+self.simulation.E0 # at CY-points
            self.E.setv = np.sqrt(n_aver(self.Ercr)**2+n_avez(self.Ezcz)**2)
            self.mue.setv = chemistry.mue_fun(self.E) # needed later for dt calculations
            self.vrcr.setv = -c_aver(self.mue)*self.Ercr
            self.vzcz.setv = -c_avez(self.mue)*self.Ezcz
            self.nui.setv = chemistry.nui_fun(self.E)
            self.nua.setv = chemistry.nua_fun(self.E)
            self.nud.setv = chemistry.nud_fun(self.E)
            self.Si.setv = self.nui*Ne            
            if self.simulation.do_diffusion:
                self.D.setv = chemistry.D_fun(self.E)
        if self.simulation.do_photoionization:
            self.photoionization.solve(self.Si) # BC automatically applied
        if self.simulation.adaptive:
            self.calculate_badness() # even if we will not find subregions
            if self.depth <= self.simulation.max_depth-2:
                self.locate_grandchildren()
        self.calculated_derived_fields = True
        # DO NOT CALCULATE BADNESS HERE! DO IT SEPARATELY!
    def recursively_prepare_vars(self,N_set):
        self.prepare_vars(N_set)
        # Skip looking for new regions for the second substep
        if self.simulation.adaptive and N_set=='phy' and self.depth < self.simulation.max_depth:
            while self.detect_new_subregions():
                pass
        for subregion in self.children:
            subregion.recursively_prepare_vars(N_set)
        # Nothing goes to the parents because only N goes to the parents      
    def calculate_badness(self,which_dxi_to_use=2):
        "self.badness: if > 1 it means our grid step is too big"
        #which_dxi_to_use = 2 # various algorithms to calculate it
        dri = required_dxi_in_region(self,0) #[which_dxi_to_use]
        dzi = required_dxi_in_region(self,1) #[which_dxi_to_use]
        # We want to avoid subregions being continuously created and immediately destroyed in cycle
        self.badness = ea_max(dri*self.dr,dzi*self.dz).view[0:end,0:end]
    def set_interp_margin(self,ea,val):
        # No bad points within self.interp_margin
        assert isinstance(ea,ExtendedArray2D)
        ea[:,0:self.interp_margin]=val
        ea[:,end-self.interp_margin:end]=val
        ea[end-self.interp_margin:end,:]=val      
    def locate_grandchildren(self):
        #r,z = self.geom.xs
        self.is_grandchild[:,:]=False
        proxy = PeriodicViewEA2D(self.is_grandchild,axis=1) \
                if self.is_global else self.is_grandchild
        for sr in self.children:
            for ssr in sr.children:
                irmax = np.int(np.ceil(ssr.rmax/self.dr))
                izmin = np.int(np.floor((ssr.zmin-self.zmin)/self.dz))
                izmax = np.int(np.ceil((ssr.zmax-self.zmin)/self.dz))
                assert izmax>izmin
                proxy[0:irmax,izmin:izmax]=True
        if self.is_global:
            # Enforce periodicity
            self.is_grandchild[:,end]=self.is_grandchild[:,0]
            # or proxy.enforce_periodicity()
    def adjusted_iz(self,iz):
        if self.parent.is_global:
            # Take into account periodicity: iz always above self.izmin
            izc = (iz - self.izmin) % self.parent.Nz + self.izmin
        else:
            izc = iz
        return izc
    def is_in(self,ir,iz,mar=np.array([0,0],dtype=np.int)):
        m0 = mar[0]; m1 = mar[1]
        ir2b = self.irmax - m0
        iz1b = self.izmin + m1
        iz2b = self.izmax - m1
        izc = self.adjusted_iz(iz)
        return (ir <= ir2b) & (izc >= iz1b) & (izc <= iz2b)
    def limit_subregion(self,irmaxtmp,izmintmp,izmaxtmp):
        """Make sure it fits inside the parent, with at least a margin of 1 (margin needed for
        interpolation)"""
        if irmaxtmp>self.Nr-self.interp_margin:
            print(WARN,self.name,': irmaxtmp=',irmaxtmp,'; Nr=',self.Nr,': too small!')
            irmax = self.Nr-self.interp_margin
        else:
            irmax = irmaxtmp
        if self.is_global:
            # Periodic, effectively infinite
            izmin = izmintmp
            izmax = izmaxtmp
        else:
            # We do not change izmax-izmin
            if izmintmp<self.interp_margin:
                izmin = self.interp_margin
                izmax = izmin + (izmaxtmp-izmintmp)
            elif izmaxtmp>self.Nz-self.interp_margin:
                izmax = self.Nz-self.interp_margin
                izmin = izmax - (izmaxtmp-izmintmp)
            else:
                izmin = izmintmp
                izmax = izmaxtmp
        return irmax,izmin,izmax
    def get_i_boundary(self,ir,iz,mar=np.array([0,0],dtype=np.int)):
        "Get the new irmax, izmin, izmax"
        return self.limit_subregion(np.max(ir)+mar[0], np.min(iz)-mar[1], np.max(iz)+mar[1])
    def detect_new_subregions(self):
        """marginsmall - the bad area + marginsmall must always stay within subregions
        marginbig - the new regions are created/moved of size bad area + marginbig.
        The badness has been pre-calculated.
        Algorithm is the following (a bit primitive, relies on the fact that bad points do not
        appear in several different locations simultaneously):
            1. If no very bad points, do nothing
            2. For each very bad point, find region which contains it.
            3. If all very bad points are within current regions, do nothing
            2. Bad points appear -> create a region around them
            3. 
        """
        # A few global preferences
        detected = False
        marginsmall = self.simulation.marginsmall
        marginbig = self.simulation.marginbig
        refiner,refinez = self.simulation.refine
        assert self.calculated_derived_fields
        assert self.simulation.adaptive
        # A shortcut
        b0,b1,b2=self.simulation.badness_boundaries
        slightly_bad = self.badness > b0
        very_bad = self.badness > b2
        med_bad = self.badness > b1
        ############################################################################################
        # Label as very bad also all the points in subsubregions
        very_bad[self.is_grandchild]=True
        med_bad[self.is_grandchild]=True
        self.set_interp_margin(very_bad,False)
        self.set_interp_margin(med_bad,False)
        self.set_interp_margin(slightly_bad,False)
        ############################################################################################
        # Erase regions that are no longer even slightly bad
        ir_s,iz_s  = ea_where(slightly_bad)
        children = self.children
        self.children = []
        for subregion in children:
            in_subregion_s = subregion.is_in(ir_s,iz_s)
            if in_subregion_s.any():
                self.children.append(subregion)
            else:
                # Not even slightly bad points here
                #print('Erasing',subregion)
                subregion.close()
                detected = True
                continue
        #self.children.sort(key=lambda r:r.num) # probably not necessary because we removed regions
        if not very_bad.arr.any():
            return detected
        # We have bad points
        ir,iz = ea_where(very_bad)
        irm,izm = ea_where(med_bad)
        assert len(ir)>0 and len(ir)==len(iz)
        ############################################################################################
        # Merge intersecting regions
        children = self.children
        self.children = []
        nch = len(children)
        is_intersecting = np.zeros((nch,),dtype=np.bool)
        for k in range(nch):
            if is_intersecting[k]:
                continue
            subr1=children[k]
            irmax = subr1.irmax; izmin = subr1.izmin; izmax = subr1.izmax
            intersect = [subr1]
            nointersect = []
            for l in range(k+1,nch):
                subr2=children[l]
                if (subr2.izmin>izmax) or (subr2.izmax<izmin):
                    # No intersection
                    nointersect.append(subr2)
                else:
                    # There is intersection
                    irmax = max(irmax,subr2.irmax)
                    izmin = min(izmin,subr2.izmin)
                    izmax = max(izmax,subr2.izmax)
                    # Mark for deletion
                    intersect.append(subr2)
                    is_intersecting[l]=True
            if len(intersect)>1:
                detected = True
                print(CC('red')+'Found intersection: [',
                      ', '.join(r.short_info() for r in intersect),']'+CC('end'))
                remaining = [children[l] for l in range(k+1,nch) if not is_intersecting[l]]
                # The number of regions goes down, so we may choose a lower available number,
                # not necesserily the number of one of the intersecting regions.
                num = get_available_num(self.children + remaining)
                newregion = self.absorb_automatic_subregions(intersect,irmax,izmin,izmax,num)
                for r in intersect:
                    assert len(r.children)==0
                    r.close()
            else:
                self.children.append(subr1)
        # self.children.sort(key=lambda r:r.num) # we merged
        ############################################################################################
        # Shrink? If no slightly bad in margin or very bad in wide margin
        children = self.children
        self.children = []
        for subregion in children:
            in_subregion = subregion.is_in(ir,iz)
            if not in_subregion.any():
                # Need to shrink around something, hope this region will disappear completely
                self.children.append(subregion)
                continue
            erased = False
            in_subregionm = subregion.is_in(irm,izm)
            ir_in = ir[in_subregion]; iz_in = iz[in_subregion]
            ir_inm = irm[in_subregionm]; iz_inm = izm[in_subregionm]
            irmax1,izmin1,izmax1 = self.get_i_boundary(ir_in,iz_in,marginsmall)
            irmax2,izmin2,izmax2 = self.get_i_boundary(ir_inm,iz_inm,marginbig)
            irmax3,izmin3,izmax3 = self.get_i_boundary(ir_in,iz_in,marginbig)
            izlen = subregion.izmax - subregion.izmin
            # Shrink in r-direction
            if irmax2 < subregion.irmax-marginbig[0] and (izmax1-izmin1) < izlen:
                detected = True
                print('Shrinking',subregion.short_info(),'from [',subregion.irmax,'x',
                    subregion.izmin,':',subregion.izmax,'] to [',irmax3,
                    'x',subregion.izmin,':',subregion.izmax,']')
                self.absorb_automatic_subregions([subregion],irmax3,subregion.izmin,subregion.izmax,
                                                 subregion.num)
                assert len(subregion.children)==0
                subregion.close()
                erased = True
                #subregion = newsubregion
                # Recalculate
                in_subregion = subregion.is_in(ir,iz)
                in_subregionm = subregion.is_in(irm,izm)
                ir_in = ir[in_subregion]; iz_in = iz[in_subregion]
                ir_inm = irm[in_subregionm]; iz_inm = izm[in_subregionm]
                irmax1,izmin1,izmax1 = self.get_i_boundary(ir_in,iz_in,marginsmall)
                irmax2,izmin2,izmax2 = self.get_i_boundary(ir_inm,iz_inm,marginbig)
                irmax3,izmin3,izmax3 = self.get_i_boundary(ir_in,iz_in,marginbig)
                izlen = subregion.izmax - subregion.izmin
            # Shrink in z-direction
            if irmax1<subregion.irmax and (izmax2-izmin2) < izlen-marginbig[1]:
                detected = True
                print('Shrinking',subregion.short_info(),'from [',subregion.irmax,'x',
                        subregion.izmin,':',subregion.izmax,'] to [',subregion.irmax,'x',
                        izmin3,':',izmax3,']')
                self.absorb_automatic_subregions([subregion],subregion.irmax,izmin3,izmax3,
                                                 subregion.num)
                assert len(subregion.children)==0
                subregion.close()
                erased = True
            if not erased:
                self.children.append(subregion)
        ############################################################################################
        # Split?
        children = self.children
        self.children = []
        for k,subregion in enumerate(children):
            in_subregion = subregion.is_in(ir,iz)
            # Adjusting iz because in a periodic domain can have a split subdomain
            iz_in = np.sort(subregion.adjusted_iz(iz[in_subregion]))
            # Check if there are any large gaps: extra marginsmall between new split regions
            split_condition = np.diff(iz_in) > 2*marginbig[1]+marginsmall[1]
            if split_condition.any():
                detected = True
                found = np.where(split_condition)[0][0]
                izlower=iz_in[found]; izupper = iz_in[found+1]
                my_utils.bigprint('SPLITTING',subregion.name,'from',izlower,'to',izupper)
                ir_in = ir[in_subregion]; iz_in = iz[in_subregion]
                in_lower = (iz_in<=izlower)
                in_upper = (iz_in>=izupper)
                assert (in_lower | in_upper).all()
                # Replace the old with lower
                irmax1,izmin1,izmax1 = self.get_i_boundary(ir_in[in_lower],iz_in[in_lower],marginbig)
                self.absorb_automatic_subregions([subregion],irmax1,izmin1,izmax1,subregion.num)
                irmax2,izmin2,izmax2 = self.get_i_boundary(ir_in[in_upper],iz_in[in_upper],marginbig)
                num = get_available_num(self.children + children[k+1:])
                self.absorb_automatic_subregions([subregion],irmax2,izmin2,izmax2,num)
                assert len(subregion.children)==0
                subregion.close()
            else:
                self.children.append(subregion)
        self.children.sort(key=lambda r:r.num)
        ############################################################################################
        # Remove points accounted for
        in_small_subregions = np.zeros(ir.shape,dtype=np.bool)
        for subregion in self.children:
            in_small_subregion = subregion.is_in(ir,iz,marginsmall)
            if in_small_subregions[in_small_subregion].any():
                # Not supposed to have overlapping "small" regions
                pass
                #raise Exception('overlapping regions')
            in_small_subregions |= in_small_subregion
        if in_small_subregions.all():
            print('Region',self.name,': all points accounted for')
            return detected
        ############################################################################################
        # New bad points in margins -- check if we can resize or move existing regions
        print(header('Region',self.name,': adding/moving/resizing subregions',color='cyan'))
        detected = True
        ir_added = ir[~in_small_subregions]
        iz_added = iz[~in_small_subregions]
        print('Region',self.name,': the unaccounted bad points are:',list(zip(ir_added,iz_added)))
        # Check if we can move the existing regions to cover them
        modified = True
        while modified:
            in_subregions = np.zeros(ir.shape,dtype=np.bool)
            children = self.children
            self.children = []
            modified = False
            for subregion in children:
                in_subregion = subregion.is_in(ir,iz)
                in_small_subregion = subregion.is_in(ir,iz,marginsmall)
                in_subregion_margin = in_subregion & (~in_small_subregion)
                if not in_subregion_margin.any():
                    # A safe margin around bad points
                    print('Subregion',subregion.short_info(),'is not modified')
                    self.children.append(subregion)
                    in_subregions |= in_subregion
                    continue
                modified = True
                print('Subregion',subregion.short_info(),': new margin points =',
                      list(zip(ir[in_subregion_margin],iz[in_subregion_margin])))
                # We have very bad points in the margin of this subregion
                # All very bad points in this large subregion
                ir_in = ir[in_subregion]; iz_in = subregion.adjusted_iz(iz[in_subregion])
                # Calculate the new position of the region
                irmax,izmin,izmax = self.get_i_boundary(ir_in,iz_in,marginsmall)
                print('New size of',subregion.short_info(),'= [',irmax,'x',izmin,':',izmax,']')
                izlen = subregion.izmax - subregion.izmin
                if  irmax > subregion.irmax or (izmax-izmin) > izlen:
                    # RESIZE
                    # Note that iz_in is "adjusted", i.e., if self.is_global, then
                    # iz_in > subregion.izmin
                    irmax,izmin,izmax = self.get_i_boundary(ir_in,iz_in,marginbig)
                    #irmax,izmin,izmax = self.limit_subregion(irmaxtmp,izmintmp,izmaxtmp)
                    print('Resizing',subregion.name,'from [',subregion.irmax,'x',
                        subregion.izmin,':',subregion.izmax,'] to [',irmax,'x',izmin,':',izmax,']')
                    newregion = self.absorb_automatic_subregions([subregion],irmax,izmin,izmax,
                                                                 subregion.num)
                    assert len(subregion.children)==0
                    subregion.close()
                    subregion = newregion
                else:
                    # We can just move it
                    self.children.append(subregion)
                    # Recenter
                    izcenter = (izmin + izmax)//2
                    newizmin = izcenter - (izlen//2)
                    if self.is_global and newizmin>=self.Nz:
                        # Wrap the periodicity
                        newizmin -= self.Nz
                    newizmax = newizmin + (subregion.izmax-subregion.izmin)
                    # Limit to the region boundaries
                    irmax1,izmin1,izmax1 = self.limit_subregion(subregion.irmax,newizmin,newizmax)
                    assert irmax1==subregion.irmax
                    if izmin1!=subregion.izmin:
                        subregion.move(izmin1)
                    else:
                        modified = False
                        return False
                in_subregions |= in_subregion
        ############################################################################################
        # Done resizing/moving, check if bad points are still left
        if in_subregions.all():
            print('Region',self.name,
                  ': all points accounted for by moving and resizing existing subregions')
            return detected
        ir_unaccounted = ir[~in_subregions]
        iz_unaccounted = iz[~in_subregions]
        print('Region',self.name,': the points that require new regions are',
              list(zip(ir_unaccounted,iz_unaccounted)))
        # Create a single new region
        # Outer size of the bad area
        irmax,izmin,izmax = self.get_i_boundary(ir_unaccounted,iz_unaccounted,marginbig)
        #irmax,izmin,izmax = self.limit_subregion(irmaxtmp,izmintmp,izmaxtmp)
        # Check if it can absorb completely any other region
        absorbed = []
        children = self.children
        self.children = []
        for subregion in children:
            if (subregion.izmin>izmax) or (subregion.izmax<izmin):
                self.children.append(subregion) # keep it
            else:
                # There is intersection
                irmax = max(irmax,subregion.irmax)
                izmin = min(izmin,subregion.izmin)
                izmax = max(izmax,subregion.izmax)
                # Mark for deletion
                absorbed.append(subregion)
        num = get_available_num(self.children)
        newregion = self.absorb_automatic_subregions(absorbed,irmax,izmin,izmax,num)
        self.children.sort(key=lambda r:r.num) # if any absorbed, must sort
        for r in absorbed:
            assert len(r.children)==0
            r.close()
        # final check
        check_unique_num(self.children)
        return detected
    def calculate_dt(self):
        dr0,dz0=self.geom.gridr.delta,self.geom.gridz.delta
        alf = self.simulation.alf # 0.9
        alfi = self.simulation.alfi # 0.5
        Npmax = np.max(self.Np.ext.arr)
        Nnmax = np.max(self.Nn.ext.arr)
        # 1/dt, "inverse", hence the name dti
        dti_ion = max(np.max(self.nui.arr),np.max(self.nua.arr),np.max(self.nud.arr),
                      max(chemistry.bete,chemistry.betn)*max(Npmax,Nnmax))
        dti_cond = phys.e*np.max((self.Ne.ext*self.mue).arr)/phys.epsilon_0
        self.dti_D = 4*np.max(self.D.arr)/min(dr0,dz0)**2
        dti_v = np.max(np.abs(self.vrcr.arr))/dr0+np.max(np.abs(self.vzcz.arr))/dz0
        print(self.name,'dt=[',myfloat(1/dti_ion),'(ion)',myfloat(1/dti_cond),'(cond)',
                             myfloat(1/self.dti_D),'(D)',myfloat(1/dti_v),'(v)]')
        dt = self.simulation.dtmax
        dt = dt if dti_ion<alfi/dt else alfi/dti_ion
        dt = dt if dti_cond<alf/dt else alf/dti_cond
        dt = dt if dti_v<alf/dt else alf/dti_v
        if self.simulation.do_explicit_diffusion or self.dti_D<=2*alf/dt:
            # Assume implicit is twice as slow as explicit
            dt = dt if self.dti_D<alf/dt else alf/self.dti_D
        assert(dt>0)
        # Diagnostic output
        vd = self.E*self.mue # drift speed
        d_D = np.min((self.D/vd).arr) # diffusion length
        d_i = np.min((vd/self.nui).arr) # ionization length
        dr,dz=self.geom.gridr.delta,self.geom.gridz.delta
        if trump.DEBUGLEVEL>1:
            print(self.name,
              '[dt=%.3g t=%.5g E=%.3g Qerr=%.3g, d_D=%.3g, d_i=%.3g, dr=%.3g, dz=%.3g] ' % \
                  (dt,self.simulation.t,np.max(self.E.arr),Qerr(self),d_D,d_i,dr,dz))
        for child in self.children:
            dt1 = child.calculate_dt()
            if dt1<dt:
                dt = dt1
        return dt
    def step_ionization(self,N_set_in,N_set_use,N_set_out,dt):
        """Now includes all ionization processes, including photoionization"""
        if self.is_global and trump.DEBUGLEVEL>2: print('[Q_ionization: ',end='')
        #Aliases
        N_phy = (self.Ne, self.Np,self.Nn)
        N_aux = (self.Ne_aux, self.Np_aux, self.Nn_aux)
        Ne_in, Np_in, Nn_in = N_phy if N_set_in=='phy' else N_aux
        N_use = N_phy if N_set_use=='phy' else N_aux
        Ne_use, Np_use, Nn_use = (N.ext for N in N_use)
        # -- can have just "ext" because it doesn't change
        Ne_out, Np_out, Nn_out = N_phy if N_set_out=='phy' else N_aux
        # Too much interdependence, use the temp first
        Ne_tmp, Np_tmp, Nn_tmp, dN = self.Ne_tmp, self.Np_tmp, self.Nn_tmp, self.dN
        Ne_tmp.setv = Ne_in.ext
        Np_tmp.setv = Np_in.ext
        Nn_tmp.setv = Nn_in.ext
        # Ionization
        if self.simulation.do_photoionization:
            dN.setv = (self.Si + self.photoionization.ptot + self.simulation.Scro \
                   - chemistry.bete*Ne_use*Np_use)*dt
        else:
            dN.setv = (self.Si + self.simulation.Scro - chemistry.bete*Ne_use*Np_use)*dt            
        Ne_tmp += dN
        # To preserve neutrality
        if self.is_global:
            correction=self.geom.integrate(Ne_tmp + Nn_in.ext - Np_in.ext)/self.geom.integrate(dN)
            print('(neutrality correction =',correction-1,')')
            self.simulation.neutrality_correction = correction
        else:
            correction=1
        Np_tmp += dN*self.simulation.neutrality_correction # now the charge is conserved
        # Attachment and detachment
        if self.simulation.do_detachment:
            dN.setv = (self.nua*Ne_use-self.nud*Nn_use)*dt
        else:
            dN.setv = self.nua*Ne_use*dt
        Nn_tmp += dN
        Ne_tmp -= dN
        # Ion recombination
        dN.setv = chemistry.betn*Np_use*Nn_use*dt
        Np_tmp -= dN
        Nn_tmp -= dN
        # Finally, update the out value
        Ne_out.ext.setv = Ne_tmp
        Np_out.ext.setv = Np_tmp
        Nn_out.ext.setv = Nn_tmp
        for child in self.children:
            child.step_ionization(N_set_in,N_set_use,N_set_out,dt)
        if trump.DEBUGLEVEL>2: print(self.name,'=',Qerr(self),', ',end='')
        for rfield in [Ne_out, Np_out, Nn_out]:
            rfield.update_parent()
            rfield.update_bc()
            rfield.apply_bc()
        if self.is_global and trump.DEBUGLEVEL>2: print(']')
    def step_advection(self,N_set_in,N_set_out,dt):
        "Not in use"
        if self.is_global and trump.DEBUGLEVEL>2: print('[Q_adv: ',end='')
        Ne_in = self.Ne if N_set_in=='phy' else self.Ne_aux
        Ne_out = self.Ne if N_set_out=='phy' else self.Ne_aux
        if N_set_in==N_set_out:
            Ne_out.ext += AdvectionStepI(
                    Ne_in.ext,self.vrcr*dt,self.vzcz*dt,self.simulation.adv_alg)
        else:
            Ne_out.ext.setv = Ne_in.ext + AdvectionStepI(
                    Ne_in.ext,self.vrcr*dt,self.vzcz*dt,self.simulation.adv_alg)
        for child in self.children:
            child.step_advection(N_set_in, N_set_out, dt)
        if trump.DEBUGLEVEL>2: print(self.name,'=',Qerr(self),', ',end='')
        Ne_out.update_parent()
        Ne_out.update_bc()
        Ne_out.apply_bc()
        if self.is_global and trump.DEBUGLEVEL>2: print(']')
    def step_diffusion(self,N_set_in,N_set_out,dt):
        "Not in use"
        if self.is_global and trump.DEBUGLEVEL>2: print('[Q_dif: ',end='')
        Ne_in = self.Ne if N_set_in=='phy' else self.Ne_aux
        Ne_out = self.Ne if N_set_out=='phy' else self.Ne_aux
        if self.simulation.do_diffusion:
            if self.simulation.do_explicit_diffusion or self.dti_D<=self.simulation.alf/dt:
                # Explicit
                if N_set_in == N_set_out:
                    Ne_out.ext += Diffusion(Ne_in.ext,self.D)*dt
                else:
                    Ne_out.ext.setv = Ne_in.ext + Diffusion(Ne_in.ext,self.D)*dt
            else:
                print(CC('red')+self.name,': implicit!'+CC('end'))
                #Ne1 = self.Ne.sol.symb.view[-1:end,-self.nlz:end]
                diff_solver = Ne_out.solver(lambda n: n-Diffusion(n,self.D)*dt)
                Ne_out.solve(diff_solver,Ne_in.sol)
        for child in self.children:
            child.step_diffusion(N_set_in, N_set_out, dt)
        if trump.DEBUGLEVEL>2: print(self.name,'=',Qerr(self),', ',end='')
        Ne_out.update_parent()
        Ne_out.update_bc()
        Ne_out.apply_bc()
        if self.is_global and trump.DEBUGLEVEL>2: print(']')        
    def step_advection_then_diffusion(self,N_set_in,N_set_out,dt):
        """Combined "step_advection" and "step_diffusion".
        Watch out the bc update in between! Important for keeping correct axis values."""
        if self.is_global and trump.DEBUGLEVEL>2: print('[Q_tra: ',end='')
        Ne_in = self.Ne if N_set_in=='phy' else self.Ne_aux
        Ne_out = self.Ne if N_set_out=='phy' else self.Ne_aux
        if N_set_in==N_set_out:
            Ne_out.ext += AdvectionStepI(
                    Ne_in.ext,self.vrcr*dt,self.vzcz*dt,self.simulation.adv_alg)
        else:
            Ne_out.ext.setv = Ne_in.ext + AdvectionStepI(
                    Ne_in.ext,self.vrcr*dt,self.vzcz*dt,self.simulation.adv_alg)
        Ne_out.apply_bc() # AXIS VALUES!!!
        # For diffusion part, the input is Ne_out
        if self.simulation.do_diffusion:
            if self.simulation.do_explicit_diffusion or self.dti_D<=self.simulation.alf/dt:
                # Explicit
                Ne_out.ext += Diffusion(Ne_out.ext,self.D)*dt
            else:
                print(CC('red')+self.name,': implicit!'+CC('end'))
                #Ne1 = self.Ne.sol.symb.view[-1:end,-self.nlz:end]
                diff_solver = Ne_out.solver(lambda n: n-Diffusion(n,self.D)*dt)
                Ne_out.solve(diff_solver,Ne_out.sol)
        for child in self.children:
            child.step_advection_then_diffusion(N_set_in, N_set_out, dt)
        if trump.DEBUGLEVEL>2: print(self.name,'=',Qerr(self),', ',end='')
        Ne_out.update_parent()
        Ne_out.update_bc()
        Ne_out.apply_bc()
        if self.is_global and trump.DEBUGLEVEL>2: print(']')
    def __repr__(self):
        g = self.geom
        r,z = g.xs
        return 'Region(id='+str(self.id)+', '+self.name+', '+\
            str(g.gridr.n_cells) + 'x' +str(g.gridz.n_cells) +\
            ', r=['+str(r[0])+', '+str(r[end])+'], z=['+str(z[0])+', '+str(z[end])+']'+\
            ', d=[' + str(myfloat(g.gridr.delta))+'x' + str(myfloat(g.gridz.delta)) + \
            '],\n\tfields='+\
            str(list(self.env.keys()))+' )'
    def close_real(self):
        for fig in self.figs:
            if fig is not None: fig.close()            
    def close(self,close_children=True):
        print(header('Closing',self.short_info(),color='blue'))
        #if self.figinitstage<2:
        #    # Figures were not plotted yet
        #    self.figs = [] # not necessary
        self.figinitstage=-1
        if close_children:
            for region in self.children:
                region.close(close_children)
    def absorb_automatic_subregions(self,oldsubregions,irmax,izmin,izmax,num):
        """This is a prefered new region creation routine. It is the most general.
        oldsubregions is a list that can be empty"""
        assert self.simulation.adaptive
        assert self.depth < self.simulation.max_depth
        #if len(oldsubregions)>0:
        ############################################################################################
        # Create the new region
        assert irmax>0 and izmax>izmin
        r,z = self.geom.xs
        if self.is_global:
            z = InfiniteGrid(self.geom.gridz,0)
        refiner,refinez = self.simulation.refine
        subdr=self.dr/refiner; subdz=self.dz/refinez
        # 1. Find an available number
        #num = self.get_available_num()
        name = self.name + '_' + str(num)
        if self.is_global:
            # if the new region completely outside, wrap it back in
            period = self.Nz
            if izmin >= period:
                izmin -= period
                izmax -= period
        newsubregion = Region(self.simulation.next_region_id,self.simulation,name,self,
                        r[irmax],z[izmin],z[izmax],subdr,subdz)
        self.simulation.next_region_id += 1
        newsubregion.num = num
        self.children.append(newsubregion)
        # Keep the children sorted (optional)
        self.children.sort(key=lambda r:r.num)
        #print(header(newsubregion,': num =',newsubregion.num,color='red',fill='-'))
        #self.simulation.regions.append(region) # is already done in the constructor
        width = 3
        newsubregion.init_plots((width,width/2*(z[izmax]-z[izmin])/r[irmax]),100*(newsubregion.id+1))
        assert newsubregion.irmax==irmax and newsubregion.izmin==izmin and newsubregion.izmax==izmax
        print(header('Absorbing [',', '.join(r.short_info() for r in oldsubregions),
                         '] into',newsubregion.short_info(),color='blue'))
        ############################################################################################
        # Copy the field values and subsubregions from old subregions to the new one
        # newsubregion.children is []
        for oldsubregion in oldsubregions:
            # 1. Copy the fields (the region of intersection)
            irmax0 = min(irmax,oldsubregion.irmax)
            izmin0 = max(izmin,oldsubregion.izmin)
            izmax0 = min(izmax,oldsubregion.izmax)
            print('Interpolating fields in area [',irmax0,izmin0,izmax0,
                  '] of new region [',irmax,izmin,izmax,']')
            for fname in oldsubregion.env:
                # Try interpolating
                ofield = oldsubregion.env[fname]
                if ofield.derived:
                    continue # only primary fields will be copied
                nfield = newsubregion.env[fname]
                # The field from parent is already set up by the Region constructor
                # Copy available values
                new_ir = refiner*irmax0
                new_iz1 = refinez*(izmin0-izmin)
                new_iz2 = refinez*(izmax0-izmin)
                old_ir = new_ir
                old_iz1 = refinez*(izmin0-oldsubregion.izmin)
                old_iz2 = refinez*(izmax0-oldsubregion.izmin)
                nfield.ext[0:new_ir,new_iz1:new_iz2]=ofield.ext[0:old_ir,old_iz1:old_iz2]
            # 2. Copy grandchildren
            unadopted = []
            for ssr in oldsubregion.children:
                success = newsubregion.adopt(ssr)
                if success:
                    newsubregion.children.append(ssr)
                else:
                    unadopted.append(ssr)
            oldsubregion.children = unadopted
        # Optional: sort
        newsubregion.children.sort(key=lambda r:r.num)
        print('Region',newsubregion.name,'has adopted children=[',
              ','.join(r.short_info() for r in newsubregion.children),']; remaining = [',
              ','.join(r.short_info()+'->{'+','.join(c.short_info() for c in r.children)+'}' \
                       for r in oldsubregions),']')
        return newsubregion
    def adopt(self,ssr):
        "self is newparent, arguments oldparent and izshift are redundant, to be removed"
        # This adoption process is complicated :|
        oldparent = ssr.parent
        # assert ssr in oldparent.children
        refiner,refinez = self.simulation.refine
        #assert izshift==refinez*(oldparent.izmin-self.izmin)
        izshift = refinez*(oldparent.izmin-self.izmin)
        # subsubregion.irmax does not change
        # First, check if we can fit the child into the parent
        if not ((ssr.zmin >= self.zmin) and (ssr.zmax <= self.zmax)):
            # Cannot adopt this child
            return False
        assert (ssr.izmin + izshift >=0) and (ssr.izmax + izshift <= self.geom.gridz.n_cells)
        old_ssr_name = ssr.name
        ssr.izmin += izshift
        ssr.izmax += izshift
        num = get_available_num(self.children)
        ssr.name = self.name + '_' + str(num)
        ssr.num = num
        ssr.parent = self
        # Switch the save directory - automatic, depends on name
        # Update creation info
        ssr.creation_info[1]=ssr.name
        ssr.creation_info[2]=self.name
        # The rest of the creation info does not change
        # The fields
        ssr.prepare_parent_updates()
        for fname,rfield in ssr.env.items():
            rfield.parent = self.env[fname]
            if not rfield.derived:
                #for fname in ['Ne','Np','Nn']:
                #rfield = ssr.env[fname]
                rfield.update_parent()
                rfield.update_bc()
                rfield.apply_bc()
        print('Moving',old_ssr_name,'from',oldparent.name,'(id=',oldparent.id,') to',
              self.name,'(id=',self.id,') as',ssr.name)
        return True
    pass

#%%#################################################################################################
class OutputIntervalChooser:
    def __init__(self,slack=2):
        self.slack=slack
        self.Emax =  [         8e6,  12e6,    16e6,  20e6,   2.5e7,   3e7,   4e7,  10e7]
        self.dtmax = [1e-10, 1e-10, 1e-10, 2.5e-11, 1e-11, 2.5e-12, 1e-12, 1e-13, 1e-14]
        self.dEmaxLim = [[0.03,0.05],[0.1e6,0.2e6]] # relative and absolute
        self.reset()
    def reset(self):
        self.stage = -1 # was: simulation.prev_stage
        self.Emax_prev = 0
    def __call__(self,Emax):
        stage = np.searchsorted(self.Emax,Emax)
        touched = False
        # Quickly changing fields
        dEmax = np.abs(Emax - self.Emax_prev)
        dEmaxR = 2*dEmax/(Emax + self.Emax_prev)
        upcondition = (stage > self.stage) or \
            (dEmaxR>self.dEmaxLim[0][1]) or (dEmax>self.dEmaxLim[1][1])
        downcondition = not ((stage + self.slack >= self.stage) or \
                             (dEmaxR>self.dEmaxLim[0][0]) or (dEmax>self.dEmaxLim[1][0]))
        if upcondition:
            touched = True
            self.stage += 1
            if self.stage>=len(self.dtmax): self.stage=len(self.dtmax)-1
        elif downcondition:
            touched = True
            self.stage -= 1
            if self.stage<0: self.stage=0
        dtmax = self.dtmax[self.stage]
        self.Emax_prev = Emax
        if touched:
            print(header('E =',myfloat(Emax/1e6),'-> dtmax =',dtmax,color='green',bold=True))
        return dtmax
    def get_const(self):
        "Parameters that are not supposed to change during the simulation"
        return [self.slack,self.Emax,self.dtmax,self.dEmaxLim]
    def set_const(self,params):
        self.slack,self.Emax,self.dtmax,self.dEmaxLim = params
    def get_state(self):
        "Return a simple array for pickling"
        return (self.stage,self.Emax_prev)
    def set_state(self,state):
        "Opposite to get_state"
        self.stage,self.Emax_prev = state

#%%#################################################################################################
# A couple of essential routines necessary for "simulation" object
def get_all_regions(region,listofregions=None):
    if listofregions is None:
        listofregions=[region]
    for child in region.children:
        listofregions.append(child)
        get_all_regions(child,listofregions)
    return listofregions
def simulation_load_barebones(dirname):
    "Load barebone info"
    with open(dirname + '/simulation.pkl','rb') as file:
        s = pickle.load(file)
    assert len(s.regions)==0
    if s.root != dirname:
        print(WARN,'Simulation was saved in',s.root,'but loaded from',dirname)
        s.root = dirname
    return s

#%%#################################################################################################
# Simulation: A class for storage of various settings
class Simulation:
    def __init__(self):
        # Will set various field manually in the run file
        self.regions=[]
        self.next_region_id=0
        self.dtmax_chooser=OutputIntervalChooser()
    def update_region_list(self):
        self.regions = get_all_regions(self.system)
    def makedir(self,name,in_root=True):
        dirname = self.root + '/' + name if in_root else name
        exists = os.path.isdir(dirname)
        if not exists:
            print(WARN,'Creating directory',dirname)
            os.makedirs(dirname)
        return exists
    def save_barebones(self):
        "Save the barebone structure of the simulation"
        assert len(self.regions)==0
        print(CC('blue')+'Saving barebone simulation to',self.root,CC('end'))
        with open(self.root + '/simulation.pkl','wb') as file:
            pickle.dump(self,file)
    def save_regions(self):
        self.makedir('dat/common')
        fname = self.root+'/dat/common/regions{:05d}.pkl'.format(self.kplot)
        tmp = [self.E0,self.dtmax_chooser.get_state(),self.next_region_id]
        for region in self.regions:
            tmp.append(region.creation_info)
        with open(fname,'wb') as file:
            pickle.dump(tmp,file)
    def load_dat(self,n):
        # We already loaded the basic structure, now load the regions
        # 1. The general region tree info
        self.kplot = n
        assert len(self.regions)==0
        fname = self.root+'/dat/common/regions{:05d}.pkl'.format(self.kplot)
        with open(fname,'rb') as file:
            tmp = pickle.load(file)
        # Decypher the tree info
        for record in tmp[3:]:
            region_id,name,parent_name,rmax,zmin,zmax,dr,dz=record
            parent=None
            for region in self.regions:
                if parent_name==region.name:
                    parent=region
                    break
            region=Region(region_id,self,name,parent,rmax,zmin,zmax,dr,dz)
            # -- this messes up self.next_region_id
            if parent is not None:
                parent.children.append(region)
            self.regions.append(region)
            if self.adaptive and parent is not None:
                # Assign a number, too
                region.num = int(name.split('_')[-1])
                num = region.num
            else:
                num = None
            print(header('Loaded',region,'number',num))
        # Other variables characterizing the state
        self.E0 = tmp[0]
        self.dtmax_chooser.set_state(tmp[1])
        self.next_region_id=tmp[2]
        #for region in self.regions:
        #    for f in region.env:
        #        region.env[f].couple_sol_to_ext()
        # 2. Load each region
        self.system = self.regions[0]
        assert self.system.is_global
        for k,region in enumerate(self.regions):
            check_unique_num(region.children)
            npz = np.load(self.root + '/dat/' + region.name + '/res{:05d}.npz'.format(n))
            #print('Region',region.name,':',npz.keys())
            if k==0:
                self.t = npz['t'][()]
                self.tnext = self.t # + self.dtmax
                self.kplot = n
            region.Ne.ext.arr[...] = npz['Ne_arr']
            region.Np.ext.arr[...] = npz['Np_arr']
            region.Nn.ext.arr[...] = npz['Nn_arr']
        self.system.recursively_prepare_vars('phy') # triggers all subregions too
    def diff_file(self,current_file_name):
        # Compare current file with the saved one
        if current_file_name is None:
            print(WARN,'Cannot compare exec file to one in',self.root)
            return True
        short_name = current_file_name.split('/')[-1]
        saved_file_name = self.root + '/' + short_name
        if not os.path.isfile(saved_file_name):
            print(WARN,'Saved Python file',short_name,'not found!')
            return True
        else:
            proc = subprocess.Popen(['diff', current_file_name, saved_file_name],
                                    stdin = subprocess.PIPE,
                                    stdout = subprocess.PIPE,
                                    stderr = subprocess.PIPE)
            (out, err) = proc.communicate()
            diff_text = out.decode()
            error_text = err.decode()
            is_different = len(diff_text)>0 or len(error_text)>0
            if is_different:
                print(WARN,'The files',short_name,'are not the same!')
            if len(diff_text)>0:
                print('Output of diff (current <=> saved):')
                print(CC('green')+out.decode()+CC('end'),end='')
            if len(error_text)>0:
                print('Errors:')
                print(CC('red')+err.decode()+CC('end'),end='')
            return is_different
    #def __repr__(self):
    #    return 'Simulation(\n' + ',\n'.join('\t'+r.__repr__() for r in self.regions) + ' )'

print('=== WELCOME TO STREAMER 2D CYL SIMULATION PACKAGE ===')

#%%
if __name__=='__main__':
    raise Exception('Please do not run this file')
    
