Streamer 2D cylindrical model and runaway electrons
===================================================
These are the supplementary files for manuscript "X-ray emissions in a multi-scale fluid model of a streamer discharge" by Nikolai G. Lehtinen and Nikolai Østgaard submitted to JGR-Atmospheres with number 2018JD028646.

The questions should be addressed to @Nikolai.Lehtinen (Nikolai.Lehtinen@uib.no).

Please use **Python3** (any version). The general-use custom package `trump` and file `my_utils.py` are available [here](https://git.app.uib.no/Nikolai.Lehtinen/TRUMP).



## The streamer model

The files required for running the fluid model of a streamer is in [`streamer/`](streamer/) directory.

The main file to run the model is [`streamer_2d_cyl_run.py`](streamer/streamer_2d_cyl_run.py). Please modify the parameters there, the default is set to $`E_0=4`$ MV/m (the first case considered in the paper).

You may have to modify [`computing_environment.py`](streamer/computing_environment.py) to set the appropriate output directory for the model.


The files [`streamer_2d_cyl_analysis.py`](streamer_2d_cyl_analysis.py) and [`plot_ne_with_subregions.py`](plot_ne_with_subregions.py) are not needed for the streamer model, only for loading and plotting the calculated results.

## Runaway electrons

The analysis files (plotting the fields, and calculation of runaway electrons) are in [`analysis/`](analysis/) directory.

