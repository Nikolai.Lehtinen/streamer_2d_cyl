# -*- coding: utf-8 -*-
"""
Created on Tue Apr 12 09:12:35 2016

The coefficients for streamer equations in air

Literature:

ML97 -- Morrow and Lowke [1997]:
Equations are:
alpha=alpha_N*N
eta=eta2_N*N+eta3_N2*N**2
dNe/dt = S + Ne*alpha*abs(We)-Ne*eta*abs(We)

P96 -- Pasko [1996] (dissertation)

GPI92 -- Glukhov, Pasko and Inan [1992]
- contains a formula for 3-body attachment as a function of T

LG12 -- Luque and Gordillo-Vázquez [2012,doi:10.1038/NGEO1314]
-- detachment

LE09 -- Luque and Ebert [2009] - coefs for Townsend approximation
ν_x = μ E_x α_x exp(−E_x/E) for ionization and attachment

We use SI units (m instead of cm in some cases!)

@author: nle003
"""

import numpy as np
import scipy.constants as phys
from my_utils import dispatch

#%% Constants and useful functions
#N0=phys.physical_constants['Loschmidt constant (273.15 K, 101.325 kPa)'][0]
N0=2.688e25 # Loschmidt constant 
Eb=3.2e6
tau0=1/1.2e8
#tau0=1/1.5e8
np.seterr(all='print')

#def safe_log10(arg):
#    return np.choose(make_dispatch(arg,[0]),[np.nan,np.log10(arg)])

#%% Electron and ion mobility/velocity
# mu_e*N - from the formula for We but to avoid infinity at E=0,
# we calculate a const value as in Pasko [1996]
def mueN_ML97(E_N):
    E_N=np.asarray(abs(E_N))
    return dispatch(E_N,
                    [1.14e-23,2.6e-21,1e-20,2e-19],
                    [3.66e25,
                     6.87e24+3.38e2/E_N,
                     7.2973e23+1.63e4/E_N,
                     1.03e24+1.3e4/E_N,
                     7.4e23+7.1e4/E_N])

def mueN_P96(E_N):
    ENN0=np.asarray(abs(E_N))*N0
    a=[50.970,3.0260,8.4733e-2]
    x=np.log10(E_N)
    return dispatch(ENN0,[0,1.62e3],[np.nan,
        1.36*N0,
        10**(a[0]+x*(a[1]+x*a[2]))])

# Must be the same as mueN*E_N
def We_ML97(E_N):
    "Electron drift speed"
    E_N=np.asarray(abs(E_N))
    return dispatch(E_N,[1.14e-23,2.6e-21,1e-20,2e-19],[
        3.66e25*E_N,
        6.87e24*E_N+3.38e2,
        7.2973e23*E_N+1.63e4,
        1.03e24*E_N+1.3e4,
        7.4e23*E_N+7.1e4])

mupN_ML97 = 2.34e-4*N0
def Wp_ML97(E_N):
    return mupN_ML97*E_N
   
def munN_ML97(E_N):
    "Previous expression divided by E/N"
    dWn0=1.86e-4 * 5e-20 * N0 - 2.7e-4 * 5e-20 * N0 # Correction to ML97 to make it continuous
    E_N=np.asarray(abs(E_N))
    return dispatch(E_N,[5e-20],
                     [1.86e-4 * N0, dWn0 / E_N + 2.7e-4 * N0])

def Wn_ML97(E_N):
    """Small ion drift speed -- watch the sign!
    The large ion speed can be 1000 times smaller [Goldman and Goldman (book), 1978, p. 221]"""
    dWn0=1.86e-4 * 5e-20 * N0 - 2.7e-4 * 5e-20 * N0 # Correction to ML97 to make it continuous
    E_N=np.asarray(abs(E_N))
    return dispatch(E_N,[5e-20],
                     [1.86e-4 * E_N * N0, dWn0 + 2.7e-4 * E_N * N0])



#%% Electron diffusion
# D*N
def TeV_ML97(E_N):
    "Added room temperature to ML97 value"
    return 3.341e8*((abs(E_N)*1e4)**0.54069)+0.026

def DN_ML97(E_N):
    return TeV_ML97(E_N)*mueN_ML97(E_N)

def nuatt3_N2_GPI92(E_N):
    # T=300
    # 1e-12*(1e-31*0.8*0.2+1.4e-29*(300/T)*np.exp(-600/T)*0.2**2)
    #T=3.341e4*((E_N*1e4)**0.54069)/phys.Boltzmann*phys.e
    #lfree=4e-7 #  # Table 2.1 in Raizer Discharge
    # average energy from Druyvestein distribution
    #T = phys.e*E_N*N0*lfree/np.sqrt(1e-4)/phys.Boltzmann # 1e-4=3m/M
    # - does not work!
    T=phys.e*TeV_ML97(E_N)/phys.Boltzmann
    return 1e-12*(1e-31*0.8*0.2+1.4e-29*(300/T)*np.exp(-600/T)*0.2**2)

#%% Ionization/attachment

# Some functions from ML97
def alpha_N_ML97(E_N):
    E_N=np.asarray(abs(E_N))
    return dispatch(E_N,[1.5e-19],[
        6.619e-21*np.exp(-5.593e-19/E_N),
        2e-20*np.exp(-7.248e-19/E_N)])

# Add an interval for small E_N to avoid negative values
def eta2_N_ML97(E_N):
    E_N=np.asarray(abs(E_N))
    return dispatch(E_N,[4.752e-20,1.05e-19],[
        0,
        6.089e-4*E_N-2.893e-23,
        8.889e-5*E_N+2.567e-23])

def eta3_N2_ML97(E_N):
    return dispatch(E_N,[0],[np.nan,
        4.7778e-69*((E_N+1e-21)*1e4)**(-1.2749)])

# Time rates
def nuion_N_ML97(E_N):
    return alpha_N_ML97(E_N)*We_ML97(E_N)

def nuatt2_N_ML97(E_N):
    return eta2_N_ML97(E_N)*We_ML97(E_N)
def nuatt3_N2_ML97(E_N):
    return eta3_N2_ML97(E_N)*We_ML97(E_N)
def nuatt_ML97(E,N):
    return nuatt2_N_ML97(E/N)*N+nuatt3_N2_ML97(E/N)*N**2

# Must make a sqrt(2) correction for const E
def nuion_N_P96(E_N):
    x=np.sqrt(2)*E_N/3.2e6*N0
    return 7.6e-19*(x**2)*(1+6.3*np.exp(-2.6/x))/1.5*np.exp(-4.7*(1/x-1))
    
def nuatt2_N_P96(E_N):
    x=E_N*N0
    y=np.log10(x)
    a=[-1073.8,465.99,-66.867,3.1970]
    b=[-2.41e8,211.92,-3.545e-5]
    return dispatch(x,[0,1.628e6,3e6],[np.nan,
        10**(a[0]+y*(a[1]+y*(a[2]+y*a[3])))/N0,
        (b[0]+x*(b[1]+x*b[2]))/N0,
        2.8166e-18])
def nuatt_P96_GPI92(E,N):
    return nuatt2_N_P96(E/N)*N+nuatt3_N2_GPI92(E/N)*N**2

def alpha_N_LE09(E_N):
    "Townsend approximation ν_x = μ E_x α_x exp(−E_x/E)"
    ENion=2e7/N0 # V/m
    alfNion = 433200/N0 # 1/m
    return alfNion*np.exp(-ENion/E_N)

def eta2_N_LG12(E_N):
    "Townsend approximation ν_x = μ E_x α_x exp(−E_x/E)"
    ENatt=6.04e6/N0 # V/m
    alfNatt = 6460/N0 # 1/m
    return alfNatt*np.exp(-ENatt/abs(E_N))

beta_ML97 = 2e-13 # Recombination Ne+Np, Nn+Np, in m^3/s

#%% Detachment
    
# O− + N_2 → N_2O + e for N_2 : O_2 composition of 80:20
def kdetach_LG12(E_N):
    a = 1.16e-18 # m^3/s
    b = 43.5e-21 # 43.5 Td
    return a*E_N**2/(b**2 + E_N**2)

def nudetach_LG12(E,N):
    return 0.8*N*kdetach_LG12(abs(E)/N)

#%% Vibrational relaxation in N2 [Lukasik and Young, 1957, doi:10.1063/1.1743947]
def N2vib_relax_ncol_LY52(T):
    "T in K, relaxation time is in number of collisions, so the rate is nuc/res."
    return 10**(4+75*(T**(-1/3)-0.06))
def N2vib_relax_time_R91(T):
    """From Reizer 1991, p. 365: the vibrational relaxation time at nearly
    normal air humidity of 8e-6 g/cm3 and T = 300 К is 60e-5 s; at 1000 K,
    it is 8e-5 s, and at 2300 K, it is 1e-5 s."""
    return np.exp(np.interp(np.log(T),
                np.log(np.array([300,1000,2300])),
                np.log(np.array([60,8,1])*1e-5),
                left=np.nan,right=np.nan))

#%%
if __name__=='__main__':
    import matplotlib as mpl
    import matplotlib.pyplot as plt
    from cycler import cycler
    #%%
    El=np.linspace(0.1,1000,1000)*1e4
    plt.figure()
    plt.semilogy(El/1e6,nuion_N_ML97(El/N0)*N0,'b')
    plt.semilogy(El/1e6,nuatt_ML97(El,N0),'g')
    plt.semilogy(El/1e6,nudetach_LG12(El,N0),'r')
    #plt.gca().set_prop_cycle(None)
    plt.semilogy(El/1e6,nuion_N_P96(El/N0)*N0,'b--')
    plt.semilogy(El/1e6,nuatt_P96_GPI92(El,N0),'g--')
    plt.ylim(1e6,1e12)
    plt.grid(True)
    plt.legend([r'$\nu_i$ [ML97]',r'$\nu_a$ [ML97]',r'$\nu_d$ [LG12]',
                r'$\nu_i$ [P96]',r'$\nu_a$ [P96]'],loc='upper left')
    plt.xlabel('$E$, MV/m')
    plt.ylabel('Rates, s$^{-1}$')
    #plt.savefig('nuiad.pdf')

    #%% Same plot, for the talk
    plt.figure()
    plt.semilogy(El,np.vstack((nuatt2_N_ML97(El/N0)*N0,nuion_N_ML97(El/N0)*N0)).T)
    plt.ylim(1e6,1e12)
    plt.grid(True)
    plt.semilogy(El,(El/Eb)**1.1/tau0,'b--')
    plt.semilogy(El,(El/Eb)**5.6/tau0,'g--')
    plt.xlabel('$E$, V/m')
    plt.ylabel(r'$\nu$, s$^{-1}$')
    plt.legend((r'$\nu_a$ [ML97]',r'$\nu_i$ [ML97]',r'$(E/E_b)^\beta/\tau_i$',
                r'$(E/E_b)^\alpha/\tau_i$'),loc='upper left')
    plt.savefig('nuai.pdf')
    #%%
    plt.figure()
    plt.plot(El,mueN_ML97(El/N0)/N0,'x-')
    plt.plot(El,mueN_P96(El/N0)/N0,'x-')
    plt.grid(True)
    #%%
    plt.figure()
    plt.plot(El,DN_ML97(El/N0)/N0,'x-')
    plt.grid(True)
    #%% Calculate d log nu/d log E
    E1=3.2e6
    dE=1e4
    E2=E1+dE
    lE1=np.log(E1)
    lE2=np.log(E2)
    lognu=lambda lE: np.log(nuion_N_ML97(np.exp(lE)/N0)*N0)
    print('dlognui/dlogE=',(lognu(lE1)-lognu(lE2))/(lE1-lE2))
    #lognu=lambda lE: np.log(nuion_N_P96(np.exp(lE)/N0)*N0)
    lognu=lambda lE: np.log(nuatt_ML97(np.exp(lE),N0))
    print('dlognui/dlogE=',(lognu(lE1)-lognu(lE2))/(lE1-lE2))
    
    #%% Photoionization psi=F*R^2
    # table from Penney and Hummert [1970]
    # R*p is in cm-Torr, thus R*p=760 corresponds to 1 cm    
    Rp=np.array([10,15,25,45,90,130,180])
    psi=np.array([5e-5,2e-5,1e-5,5e-6,2e-6,1e-6,5e-7])
    plt.figure()
    plt.semilogy(Rp,psi)
    plt.grid(True)
    K1=5e-5; Lp1=5;
    K2=5e-7; Lp2=30;
    psit=Rp*(K1*np.exp(-Rp/Lp1) + K2*np.exp(-Rp/Lp2))
    print('C1=',4*np.pi*Lp1**2*K1,', C2=',4*np.pi*Lp2**2*K2)
    plt.semilogy(Rp,psit)
    
