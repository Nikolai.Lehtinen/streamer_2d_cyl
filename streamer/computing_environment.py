#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 30 18:03:01 2018

@author: nle003
"""

import socket
import os
host = socket.gethostname()
print('Running on',socket.gethostbyaddr(host)[0])
HPC_BATCH = False
if host == 'nleht-xps13':
    DATA = '/home/nleht/Documents/VirtualPython3/sci/'
elif host=='ift039113':
    DATA = '/scratch2/python/'
else:
    HPC_BATCH = True
    # We are running from exec directory, change up
    DATA = os.path.dirname(os.getcwd()) + '/data/'
