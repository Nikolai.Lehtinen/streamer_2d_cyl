#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr 21 17:51:00 2017

Photoionization
---------------
Representation of photoionization in air at sea level as a combination
of Helmholtz equation solutions.

Usage:
    import photoionization

Theory details:
    
Zheleznyak et al [1982]:
    kappa_max = 2
    kappa_min = 3.5e-2 (mmHg*cm)^(-1)
We notice that log(kappa_max/kappa_min)=4

Converting this to Lam:
    kappa*760*0.2 = 1/Lam(cm)
    Lam(m) = 1/(kappa*760*0.2)*1e-2
    Lam_max = 1/(kappa_min*760*0.2)*1e-2 = 0.002 m
    Lam_min = Lam_max/exp(4)

The normzalizing coefficient
A = pt/(p+pt) * (xi*w/alf) where pt = 30 mmHg [ref] or 60 mmHg [ref]
(xi*w/alf) is given in the table in Zheleznyak et al and is of the order of 0.1

@author: nle003
"""

#%% Preliminaries
import numpy as np
import matplotlib.pyplot as plt
from my_utils import dispatch, meandrize, duplicate

##%% New
## 5 intervals
## Assume log(Lam1/Lam2)=4  and take boundaries Lam2*exp(k), k = 0,1,2,3
#Lam_max=2e-3
#Lam_min=Lam_max/np.exp(4.)
#Lam_b=Lam_min*np.exp(np.arange(-7.,5.)); Lam_b[0]=0
##CPhot=0.1*(60/(760+60))
#CPhot = 1.
#C=CPhot*(1./4.)*np.array([
#        np.exp(-6),
#        np.exp(-5)-np.exp(-6),np.exp(-4)-np.exp(-5),np.exp(-3)-np.exp(-4),
#        np.exp(-2)-np.exp(-3),np.exp(-1)-np.exp(-2),1-np.exp(-1),
#        1-np.exp(-3.),1-np.exp(-2)+np.exp(-3),
#        1-np.exp(-1.)+np.exp(-2.),np.exp(-1.)])
##LamPhot=LamBPhot[1:]
#Nterms=len(C)
#LamPhot=(Lam_b[:-1]+Lam_b[1:])/2.
##LamPhot[0]=LamBPhot[1]/3

#%% New
# 5 intervals
# Assume log(Lam1/Lam2)=4  and take boundaries Lam2*exp(k), k = 0,1,2,3
Lam1Phot=1.9e-3
Lam2Phot=Lam1Phot/np.exp(4.)
LamBPhot=Lam2Phot*np.exp(np.hstack((np.arange(-9.,0.,3),np.arange(0.,5.,2,))));
#CPhot=0.1*(60/(760+60))
CPhot = 1.
C=CPhot*(1./4.)*np.array([
        np.exp(-6),
        np.exp(-3)-np.exp(-6),1-np.exp(-3),
        2-np.exp(-2.),
        1+np.exp(-2.)])
#LamPhot=LamBPhot[1:]
Nterms=len(C)
LamPhot=(LamBPhot[:-1]+LamBPhot[1:])/2.
#LamPhot[0]=LamBPhot[1]/3

#%% The normalized kernels
def g1(lam,r):
    """Solution of Helmholtz equation (i.e., diffusion approximation).
    Normalized to 1."""
    return np.exp(-r/lam)/(4*np.pi*r*lam**2)
def g2(lam,r):
    """The photons propagate straight away from the source with attenuation.
    Normalized to 1."""
    return np.exp(-r/lam)/(4*np.pi*r**2*lam)
def g3(lam1,lam2,r):
    "Zheleznyak et al [1982] function, normzalized to 1. Must have lam1>lam2"
    assert(lam1>lam2)
    return (np.exp(-r/lam1)-np.exp(-r/lam2))/(4*np.pi*r**3*np.log(lam1/lam2))

def weight_fun(lam1,lam2,lam):
    assert lam1>lam2
    return dispatch(lam,[0,lam2,lam1],[np.nan,1/lam2-1/lam1,1/lam-1/lam1,0])/np.log(lam1/lam2)

#%% Approximate
def zheleznyak_approx(r):
    res = np.zeros(r.shape)
    for k in range(Nterms):
        res += C[k]*g1(LamPhot[k],r)
    return res

# Exact
def zheleznyak(r):
    "Zheleznyak et al [1982] function, with concrete values of lam1 and lam2."
    return g3(Lam1Phot,Lam2Phot,r)

#%%
if __name__=='__main__':
    #%%
    if False:
        plt.figure(100)
        plt.clf()
        ll=np.linspace(0,Lam1Phot,1000)
        plt.loglog(ll,weight_fun(Lam1Phot,Lam2Phot,ll))
        dLam = np.diff(LamBPhot)
        plt.plot(meandrize(LamBPhot),duplicate(C/dLam))
        plt.plot(LamPhot,C/dLam,'o')
    #weight_fun(Lam1Phot,Lam2Phot,LamPhot),'x')
    #%%
    r=10**np.linspace(-8,-1.5)
    fig = plt.figure(89,figsize=(7,5))
    fig.clf()
    ax=fig.gca()
    select_p = [2,3,4]
    C1 = C.copy()
    #C1[3] *= 5
    norm = np.sum(C[select_p])
    line,=ax.loglog(r/1e-3,sum(C1[ii]*g1(LamPhot[ii],r) for ii in select_p)/norm)
    line.set_label('sum of selected terms')
    #for i1 in range(5):
    #    #ax.loglog(r/1e-3,sum(C[ii]*g1(LamPhot[ii],r) for ii in range(i1))) # /sum(C[:i1]))
    #    ax.loglog(r/1e-3,C[i1]*g1(LamPhot[i1],r)) 
    line,=ax.loglog(r/1e-3,zheleznyak(r))
    line.set_label('Zheleznyak et al [1982]')
    line,=ax.loglog(r/1e-3,zheleznyak_approx(r))
    line.set_label('Approximate ('+str(len(C))+' terms)')
    ax.set_ylim(1e-5,1e20)
    ax.set_xlabel('$r, mm$')
    ax.set_ylabel('$F_Z(r), m^{-3}$')
    ax.legend(loc='upper right')
    ax.grid(True)
    if False:
        fig.savefig('zheleznyak.pdf')
    
