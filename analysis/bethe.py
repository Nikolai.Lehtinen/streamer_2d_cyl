#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Feb 10 15:08:55 2018

@author: nle003
"""

#%%
import pickle
import numpy as np
import scipy.constants as phys
import scipy.interpolate
import matplotlib.pyplot as plt
from my_utils import string, ave, myfloat, ones_as

#%% Determine if this is a package or executable
try:
    print('Executable =',__file__)
except NameError:
    print('Executable = main')
    running_as_main = True
else:
    running_as_main = False
do_exec = running_as_main or __name__=='__main__'
print('do_exec=',do_exec)
if do_exec:
    save_figures = False

#%% Bethe stuff
mc2 = phys.m_e*phys.c**2
mc = phys.m_e*phys.c
e2 = phys.e**2/(4*np.pi*phys.epsilon_0)
r0 = e2/mc2 # classical radius
Zm = 14.5
Za = Zm/2
No = 2.688e+25
nu0 = 2*np.pi*No*r0**2*phys.c

def pmc_from_keV(En):
    return np.sqrt((1+En*1e3*phys.e/mc2)**2-1)
def keV_from_pmc(pmc):
    return (np.sqrt(1+pmc**2)-1)*mc2/phys.e/1e3

#def kinet(En):
#    gam=1+En/mc2
#    b = np.sqrt(1-1/gam**2)
#    p = mc*b*gam
#    return b,gam,p

EneV_limit = 1000 # below which formulas are not correct
gam_limit = 1+EneV_limit*phys.e/mc2
pmc_limit = np.sqrt(gam_limit**2-1)
def FD(pmc):
    """Dynamic friction as a function of momentum. Argument is dimensionless p/mc.
    See Lehtinen dissertation, equation (2.2)"""
    #b,gam,p = kinet(En)
    assert (np.asarray(pmc)>=pmc_limit).all()
    gam = np.sqrt(1+pmc**2)
    b = pmc/gam
    Eionmc2 = 80.5*phys.e/mc2
    res = np.log(b**2*(gam-1)*gam**2/(Eionmc2**2)) - (1+2/gam-1/gam**2)*np.log(2) \
        + (gam-1)**2/8/gam**2 + 1/gam**2
    return nu0*Zm*mc*res/b**2

def nu_FD(pmc):
    "Dynamic friction as a time rate, i.e. F_D/p"
    #b,gam,p = kinet(En)
    gam = np.sqrt(1+pmc**2)
    b = pmc/gam
    Eionmc2 = 80.5*phys.e/mc2
    assert (np.asarray(gam)>=1+1000*phys.e/mc2).all()
    res = np.log(b**2*(gam-1)*gam**2/(Eionmc2**2)) - (1+2/gam-1/gam**2)*np.log(2) \
        + (gam-1)**2/8/gam**2 + 1/gam**2
    return nu0*Zm*res/b**2/pmc

def nu_m(pmc):
    "Momentum loss rate: Equation (2.13) in Lehtinen dissertation divided by 2"
    gam=np.sqrt(1+pmc**2)
    res = nu0*Zm**2*gam/pmc**3*np.log(164.7*pmc/Zm**(1/3))
    return res

#def pnu(En):
#    b,gam,p=kinet(En)
#    return p*nu_m(En)

def mu_ave(eEpnu):
    """Based on equation (2.25) in Lehtinen dissertation.
    The argument is e*E/(p*nu_m)."""
    assert (np.asarray(eEpnu)>=0).all()
    a = 2*eEpnu
    return 1/np.tanh(a)-1/a
    #ep = np.exp(a)
    #em = 1/ep
    #return ((ep+em)-(ep-em)/a)/(ep-em)

#%% Determine the break-even field as a function of pmc
Fs = 10**np.linspace(5,8,1000)*phys.e
@np.vectorize
def get_Feff(pmc):
    "Solve equation eE*M(p,eE)=FD(p) for eE"
    eEpnu = Fs/(pmc*mc*nu_m(pmc))
    F = FD(pmc)/mu_ave(eEpnu)
    return np.interp(0,Fs-F,Fs)

#%% Bremsstrahlung cross-sections
# See Koch and Motz [1959]
def bremsstrahlung_3BN(pmc,k):
    """Differential cross-section dchi/dk at electron energy Ene PER MOLECULE (x2)!
    Per unit k!!!
    The dimensionless variables are:
        pmc = p_e/(mc) -- initial electron momentum;
        k = En_phot/(mc2) = p_phot/(mc) -- photon energy (or momentum) (can be table)
    Approximations: Born, Nonscreened"""
    chi0 = 2*Za*(Za+1)*r0**2/137 # factor of 2 because molecule
    γ = np.sqrt(1+pmc**2)
    assert (k<γ-1).all() and (k>0).all()
    γ1 = γ-k # final electron gamma-factor
    γγ1 = γ*γ1
    pmc1 = np.sqrt(γ1**2-1)
    pp1 = pmc*pmc1
    L = 2*np.log((γγ1 + pp1-1)/k)
    z = 2*np.log(γ+pmc)
    z1 = 2*np.log(γ1+pmc1)
    chi1 = (4/3)-2*γγ1*(pmc**2+pmc1**2)/pp1**2 + z*γ1/pmc**3 + z1*γ/pmc1**3 - z*z1/pp1
    chipart = z*(γγ1+pmc**2)/pmc**3 - z1*(γγ1+pmc1**2)/pmc1**3 + 2*k*γγ1/pp1**2
    chi2 = (8/3)*γγ1/pp1 + k**2*(γγ1**2 + pp1**2)/pp1**3 + k*chipart/2/pp1
    return chi0*pmc1/pmc*(chi1+L*chi2)/k

if do_exec:
    # 1 MeV electrons, 80 keV photons
    EnekeV = 1000
    EnphkeV = np.linspace(0,EnekeV,1000)
    EnphkeVc = ave(EnphkeV)
    kb = EnphkeV*1000*phys.e/mc2 # dimensionless photon energy
    kc = ave(kb)
    dk = np.diff(kb)
    pmc = pmc_from_keV(EnekeV)
    v = pmc*phys.c/np.sqrt(1+pmc**2)
    chi = bremsstrahlung_3BN(pmc,kc) # per molecule per unit k
    tmp = No*v*np.cumsum(chi*dk); rate = tmp[-1]-tmp
    plt.figure(600)
    plt.clf()
    nu_eff = get_Feff(pmc)/pmc/mc
    plt.plot(EnphkeVc,rate/nu_eff)
    EnphkeV0 = 100
    ii = np.searchsorted(EnphkeVc,EnphkeV0)
    print('Probability of a',EnekeV,'keV electron to produce',EnphkeV0,'keV photon',
          'before slowing down is',rate[ii]/nu_eff)
    
#%%#################################################################################################
#%% DERIVED STUFF, MAYBE MOVE TO ANOTHER FILE


#%% Plot
Ens = 10**np.linspace(3,7.6,1000)*phys.e
pmc = np.sqrt((1+Ens/mc2)**2-1)
# Reusable table values
FDs = FD(pmc)
pnus = pmc*mc*nu_m(pmc)
Feffs = get_Feff(pmc)
def get_ps(E):
    "Solve equation eE*M(p,eE)=FD(p) for p and return p/mc"
    ii=np.argmin(Feffs)
    return np.interp(E,Feffs[ii-1::-1]/phys.e,pmc[ii-1::-1])
def get_pFD(E):
    "Solve equation eE=FD(p) for p and return p/mc"
    ii=np.argmin(FDs)
    return np.interp(E,FDs[ii-1::-1]/phys.e,pmc[ii-1::-1])

def save_fig_in_pdf_and_pkl(fig,fname):
    fig.savefig(fname+'.pdf')
    with open(fname+'.pkl','wb') as f:
        pickle.dump(fig,f)
    
if do_exec:
    fig=plt.figure(30)
    plt.clf()
    line,=plt.loglog(Ens/phys.e,FDs/phys.e)
    line.set_label(r'dynamic friction $F_D$')
    line,=plt.loglog(Ens/phys.e,pnus/phys.e)
    line.set_label(r'momentum loss $\nu_m p$')
    line,=plt.loglog(Ens/phys.e,Feffs/phys.e)
    line.set_label(r'effective friction $F_{eff}$')
    plt.xlabel(r'$\mathcal{E}$, eV')
    plt.ylabel(r'Force, V/m')
    plt.grid(True,which='both')
    plt.legend()
    #plt.plot(Ens/phys.e,3e5*pmc**(-2))
    save_fig_in_pdf_and_pkl(fig,'frictions')

#%%
if do_exec:
    # Friction ratios
    fig=plt.figure(33)
    plt.clf()
    line,=plt.loglog(Ens/phys.e,pnus/FDs)
    line.set_label(r'$\nu_m p/F_D$')
    line,=plt.loglog(Ens/phys.e,Feffs/FDs)
    line.set_label(r'$F_{eff}/F_D$')
    plt.xlabel(r'$\mathcal{E}$, eV')
    plt.ylabel(r'Force, V/m')
    plt.grid(True,which='both')
    plt.legend()
    #plt.plot(Ens/phys.e,3e5*pmc**(-2))
    save_fig_in_pdf_and_pkl(fig,'friction_ratios')
    

#%% Weird stuff
if do_exec:
    plt.figure(31)
    plt.clf()
    # The average cosine for the given p
    M = mu_ave(Feffs/pnus)
    plt.semilogx(Feffs/phys.e,M)
    plt.grid(True,which='both')

#%% Weird stuff 2
# A different runaway boundary -- almost two times different
Fles = np.sqrt(3*FDs*pnus/5)
if do_exec:
    plt.figure(30)
    line,=plt.plot(Ens/phys.e,Fles/phys.e)
    line.set_label(r'low-energy result (inexact)')
    plt.legend()

#%% Probability of runaway
def get_prob_escape(E0,do_approx=False):
    "Exact; pmc_table has to be dense enough to tolerate integration"
    dpmc = np.diff(pmc)
    pmcc = ave(pmc)
    if do_approx:
        # This one is actually more complicated
        ii = np.where(Fles/phys.e<E0)[0][0]
        pn = pmcc/pmc[ii]
        diff_prob = np.exp(-(5/4)/pn**4)/pn**5
    else:
        Dcs = (phys.e*E0)**2/(3*nu_m(pmcc))
        tmp = np.hstack((0,np.cumsum(FD(pmcc)/Dcs*dpmc*mc)))
        tmp -= tmp[-1] # so that the exponent is not huge
        diff_prob = np.exp(ave(tmp))/(pmcc**2*Dcs) # expression to be integrated
    tmp = np.hstack((0,np.cumsum(diff_prob*dpmc)))
    return tmp/tmp[-1]

# Tabulate it
prob2d = np.zeros((len(Fs),len(pmc)))
for k,E0 in enumerate(Fs/phys.e):
    prob2d[k,:] = get_prob_escape(E0,do_approx=False)

prob_escape = scipy.interpolate.interp2d(pmc, Fs/phys.e, prob2d)

#%%
if do_exec:
    #dpmc = np.diff(pmc)
    #pmcc = ave(pmc)
    #pmc_s = 0.7 # approximate for E=5 Et
    E0 = 1e6 # E=5 Et, like in Figure 2.6 in Lehtinen dissertation
    ii = np.where(Fles/phys.e<E0)[0][0]
    jj = np.where(Feffs/phys.e<E0)[0][0]
    #tmp = np.hstack((0,np.cumsum(diff_prob_approx(pmcc,pmc[ii])*dpmc)))
    #prob_escape = tmp/tmp[-1]
    plt.figure(32)
    plt.clf()
    prob_escape_approx = get_prob_escape(E0,pmc,do_approx=True)
    line,=plt.semilogx(Ens/phys.e,prob_escape_approx)
    line.set_label('approximate')
    plt.semilogx(Ens[ii]/phys.e,prob_escape_approx[ii],'o')
    plt.semilogx(Ens[jj]/phys.e,prob_escape_approx[jj],'x')
    plt.xlabel(r'$\mathcal{E}$, eV')
    plt.ylabel(r'Probability of runaway')
    pmc1,pmc2 = np.interp(np.array([0.1,0.9]),prob_escape_approx,pmc)
    plt.title(string('E=',E0,'V/m: runaway boundary is p/(mc) from',myfloat(pmc1),'to',myfloat(pmc2)))
    plt.grid(True)
    # "Exact"
    #Dcs = (phys.e*E0)**2/(3*nu_m(pmcc))
    #tmp = np.hstack((0,np.cumsum(FD(pmcc)/Dcs*dpmc*mc)))
    #tmp -= tmp[-1]
    #arg = np.exp(ave(tmp))/(pmcc**2*Dcs)
    #tmp = np.hstack((0,np.cumsum(arg*dpmc*mc)))
    #prob_escape_exact = tmp/tmp[-1]
    prob_escape_exact = get_prob_escape(E0,pmc,do_approx=False)
    line,=plt.semilogx(Ens/phys.e,prob_escape_exact)
    line.set_label('exact')
    plt.semilogx(Ens[ii]/phys.e,prob_escape_exact[ii],'o')
    plt.semilogx(Ens[jj]/phys.e,prob_escape_exact[jj],'x')
    plt.legend()
    if save_figures:
        plt.savefig('prob_escape.pdf')

#%%
if do_exec:
    plt.figure(33)
    plt.pcolor(np.log10(Ens[::10]/phys.e),np.log10(Fs[::10]/phys.e),prob2d[::10,::10])
    plt.plot(np.log10(Ens/phys.e),np.log10(Fles/phys.e),'r')
    plt.plot(np.log10(Ens/phys.e),np.log10(Feffs/phys.e),'g')
 