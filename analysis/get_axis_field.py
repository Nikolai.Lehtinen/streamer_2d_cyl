#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb  6 22:18:06 2018

@author: nleht
"""

#%%
import os
import os.path
import numpy as np
import scipy.constants as phys
import scipy.interpolate
from scipy.integrate import odeint
import matplotlib.pyplot as plt
#import chemistry
from nleht_utils import string, myfloat
from trump import end,span
from trump_2D import ea_plot, ea_plot_slice
from streamer_2d_cyl import simulation_load_barebones, rootdir
from streamer_2d_cyl_analysis import RegionInfo, load_all_region_fields, load_t_array, \
    ea_plot_slice_lim, locate_children, load_regions, load_region_fields
from bethe import mc2, mc, FD, nu_FD, nu_m, mu_ave, prob_escape

def read_axis_field(adir,E0):
    simulation=simulation_load_barebones(rootdir+adir)
    simulation.E0=E0
    bfname = rootdir + adir + '/Eaxis.npz'
    if not os.path.isfile(bfname):
        print('Doing a long calculation to be saved in',bfname)
        ts = load_t_array(simulation)
        Ez = None
        Nplot = len(ts)
        for kplot in range(Nplot):
            if kplot%50==0:
                print(kplot,'/',Nplot)
            rs,E0 = load_regions(simulation,kplot)
            if rs is None: continue
            r = rs[0]
            assert r.name=='system'
            t = load_region_fields(r,simulation,kplot)
            assert t==ts[kplot]
            Ez1 = r.Ezcz
            if Ez is None:
                L = r.zmax - r.zmin
                zE = Ez1.x(1)
                Nz = len(zE)
                Ez=np.zeros((Nplot,Nz))
            Ez[kplot,:]=Ez1[0,:]
        np.savez_compressed(bfname,ts=ts,zE=zE,Ez=Ez,L=L)
    else:
        print('Loading',bfname)
        tmp = np.load(bfname)
        ts = tmp['ts']
        zE = tmp['zE']
        Ez = tmp['Ez']
        L = tmp['L'][()]
    return simulation, ts, zE, Ez, L

# Try solving a motion equation
# dpz/dt = -e*E*M(p,E) - nu_FD(p)*pz
# dz/dt = pz*c*M(p,E)/sqrt(mc**2 + p**2)
def solve_electron_motion(En0keV,method):
    "Global vars used: z0, Ez_fun, t0, trange"
    assert method in ['free','fric','fric+elas']
    pmc0 = np.sqrt((1+En0keV*1000*phys.e/mc2)**2-1)
    pmc_stop = np.sqrt((1+1000*phys.e/mc2)**2-1) # stop when energy is <1keV
    tt = np.linspace(t0,t0+trange,500) #ts[it:] #t0 + np.linspace(0,1e-10,100)
    y0 = np.array([z0,-pmc0])
    zpsol = np.full((len(tt),3),np.nan)
    dt = np.diff(tt)
    zpsol[0,:2]=y0
    pmcz= -pmc0
    z = z0
    do_elas = (method=='fric+elas')
    do_fric = (method=='fric+elas' or method=='fric')
    stopped=False
    for k,t in enumerate(tt[:-1]):
        E=Ez_fun(z,t)[0]
        F_E = -phys.e*E/mc # Electric force, in s^{-1}
        #En = mc2*(np.sqrt(1+pmcz**2)-1)
        pmc = np.abs(pmcz)
        zpsol[k,2]=prob_escape(pmc,np.abs(E))
        if pmc>pmc_stop:
            # Electron is not stopped
            if do_elas:
                eEpnu = F_E/(pmcz*nu_m(pmc))
                assert eEpnu>0
                mu = mu_ave(eEpnu)
            else:
                mu = 1
            pmcz += F_E*mu*dt[k]
            if do_fric:
                pmcz /= (1+nu_FD(pmc)*dt[k])
            vz = pmcz*phys.c/np.sqrt(1+pmc**2)
            z += vz*mu*dt[k]
        else:
            # Electron is stopped
            pmcz = 0
            stopped=True
            # z is not changed
        zpsol[k+1,:2]=(z,pmcz)
    return stopped,tt,zpsol

#%%
#simulation, ts, zE, Ez, L = read_axis_field('midpoint_fine_smallphoto_smallcr/E_4MVm', 4e6)
#thetitle = r'$E_0=4$ MV/m, low ionization'
simulation, ts, zE, Ez, L = read_axis_field('midpoint_fine/E_4MVm', 4e6)
thetitle = r'$E_0=4$ MV/m'
#simulation, ts, zE, Ez, L = read_axis_field('/midpoint_fine_almostnoion/E_4MVm', 4e6)
#simulation, ts, zE, Ez, L = read_axis_field('/midpoint_fine_almostnoion/E_4MVm',4e6)
#thetitle = r'$E_0=4$ MV/m, very low phot/no cr'
# Double it
dz = L/len(zE)
zE2 = np.hstack((zE,zE+L))
Ez2 = np.hstack((Ez,Ez))
phi2 = np.cumsum(Ez2*2e-5,axis=1)
Ez_fun = scipy.interpolate.interp2d(zE2, ts, Ez2)

#%%
it,iz=np.unravel_index(np.argmax(Ez2),Ez2.shape)
t0 = ts[it]
z0 = zE[iz]
#plt.figure(1)
#plt.clf()
Ethermal = 10e6 # this is a bit arbitrary
dphi=np.zeros((len(ts),))
for kt in range(it-10,min(it+11,len(ts))):
    above=(Ez[kt,:]-Ethermal)>0
    ii=np.where(above)[0]
    dphi[kt]=np.sum(Ez[kt,ii]*dz)
    #print('dphi=',dphi)
#plt.plot(ts,dphi)
EnkeV0=np.max(dphi)/1e3
print('The maximum available energy is',EnkeV0,'keV')
#plt.plot(zE2,phi2[it-3:it+4,:].T)
#plt.grid(True)

#%%
#plt.plot(ts,z0+phys.c*(ts-t0),'w')
#v4keV = np.sqrt(2*4000*phys.e/phys.m_e)
#plt.plot(ts,z0+v4keV*(ts-t0),'w')
#gam = 1+4000*phys.e/(phys.m_e*phys.c**2)
#v4keV = phys.c*np.sqrt(1-1/gam**2)
#plt.plot(ts,z0-v4keV*(ts-t0),'w')
trange = 2e-10
zrange1 = 2e-3
zrange2 = 1e-3
#itch = ((ts >= t0-trange) & (ts <= t0+trange))
#izch = ((zE2 >= z0-zrange) & (zE2 <= z0+zrange))
#itch = np.where((ts >= t0-trange) & (ts <= t0+trange))[0]
#izch = np.where((zE2 >= z0-zrange) & (zE2 <= z0+zrange))[0]
it1,it2 = np.searchsorted(ts,[t0-trange,t0+trange])
its = slice(it1,it2+1)
iz1,iz2 = np.searchsorted(zE2,[z0-zrange1,z0+zrange2])
izs = slice(iz1,iz2+1)
plt.figure(10)
plt.clf()
plt.pcolormesh(ts[its]/1e-9,zE2[izs]/1e-3,Ez2.T[izs,its]/1e6)
plt.xlim((t0-trange)/1e-9,(t0+trange)/1e-9)
plt.ylim((z0-zrange1)/1e-3,(z0+zrange2)/1e-3)
plt.xlabel(r'$t$, ns')
plt.ylabel(r'$z$, mm')
plt.title(thetitle)
cb = plt.colorbar()
cb.set_label(r'$E$, MV/m')


#%%
plt.figure(10)
stopped,tt,zpsol = solve_electron_motion(EnkeV0,'fric+elas')
line,=plt.plot(tt/1e-9,zpsol[:,0]/1e-3,'w',linewidth=1)
line.set_label('{:.2f} keV'.format(EnkeV0))
EnkeV = np.floor(EnkeV0)
while stopped:
    EnkeV+=.1
    stopped,tt,zpsol2 = solve_electron_motion(EnkeV,'fric+elas')
#for EnkeV in [10,15,20,25,30]:
#    tt,zpsol2 = get_solution(EnkeV,'fric+elas')
line,=plt.plot(tt/1e-9,zpsol2[:,0]/1e-3,'r',linewidth=1)
line.set_label('Runaway: '+str(myfloat(EnkeV))+' keV')
plt.legend(loc='lower left')
#plt.savefig('running_electron.pdf')

##%%
#plt.figure(11)
#plt.plot((tt-t0)/1e-9,zpsol[:,2])