#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 31 16:00:27 2018

Plot Ne with subregions

@author: nle003
"""

#%%
import numpy as np
import matplotlib.pyplot as plt
from my_utils import myfloat
from my_utils import string
from streamer_2d_cyl import simulation_load_barebones, rootdir, rectangle
from streamer_2d_cyl_analysis import RegionInfo, load_all_region_fields, \
    load_t_array, ea_plot_slice_lim
from trump.trump_2D import UpdateablePcolorAxis
from trump import span, end

#%%#################################################################################################
simulation = simulation_load_barebones(rootdir + '/midpoint_fine/E_4MVm')
simulation.E0 = 4e6
ts = load_t_array(simulation)


#%%

kplots=np.arange(0,1000,100)
fig,axs = plt.subplots(2,5,figsize=(10,9))
axs.resize((10,))
fig.tight_layout()

uaxs=np.array([UpdateablePcolorAxis(axs[k],scale=0.001,pseudo=False) for k in range(10)])

#%%
plot_regions=True
for k,kplot in enumerate(kplots):
    rs = load_all_region_fields(simulation,kplot)
    system = rs[0]
    assert system.name=='system'
    uaxs[k].plot(np.log10(system.Ne+1),title=str(myfloat(ts[kplot]/1e-9))+' ns')
    ax = uaxs[k].ax
    assert ax is axs[k]
    if k==0 or k==5: ax.set_ylabel(r'$z$, mm')
    if k>=5: ax.set_xlabel(r'$r$, mm')
    if plot_regions:
        for r in rs[1:]:
            bx,by = rectangle(r.rmax,r.zmin,r.zmax,1e-3)
            ax.plot(bx,by,'w')
fig.savefig('regions.pdf')

#%%#################################################################################################
simulation = simulation_load_barebones(rootdir + '/midpoint_fine/E_6MVm')
simulation.E0 = 6e6
ts = load_t_array(simulation)


#%%

kplots=np.arange(0,50,5)
fig,axs = plt.subplots(2,5,figsize=(10,9))
axs.resize((10,))
fig.tight_layout()

uaxs=np.array([UpdateablePcolorAxis(axs[k],scale=0.001,pseudo=False) for k in range(10)])

#%%
plot_regions=False
for k,kplot in enumerate(kplots):
    rs = load_all_region_fields(simulation,kplot)
    system = rs[0]
    assert system.name=='system'
    uaxs[k].plot(np.log10(system.Ne+1),title=str(myfloat(ts[kplot]/1e-9))+' ns')
    ax = uaxs[k].ax
    assert ax is axs[k]
    if k==0 or k==5: ax.set_ylabel(r'$z$, mm')
    if k>=5: ax.set_xlabel(r'$r$, mm')
    if plot_regions:
        for r in rs[1:]:
            bx,by = rectangle(r.rmax,r.zmin,r.zmax,1e-3)
            ax.plot(bx,by,'w')
fig.savefig('regions.pdf')

#%%#################################################################################################
simulation = simulation_load_barebones(rootdir + '/midpoint_fine/E_4MVm')
simulation.E0 = 4e6
ts = load_t_array(simulation)
tplots = np.array([10.,20.,25.,27.,28.])*1e-9
#kplots = np.searchsorted(ts,tplots) #,side='right')
kplots_double = np.interp(tplots,ts,np.arange(len(ts)))
kplots = np.rint(kplots_double).astype(np.int)
Nplots = len(kplots)

#kplots=np.arange(0,50,5)
fig,axs = plt.subplots(1,Nplots,figsize=(11,6))
fig.tight_layout()

uaxs=np.array([UpdateablePcolorAxis(axs[k],scale=0.001,pseudo=False) for k in range(Nplots)])

#%%
plot_regions=False
for k,kplot in enumerate(kplots):
    rs = load_all_region_fields(simulation,kplot)
    system = rs[0]
    assert system.name=='system'
    uaxs[k].plot(np.log10(system.Ne+1),title=str(myfloat(ts[kplot]/1e-9))+' ns')
    ax = uaxs[k].ax
    assert ax is axs[k]
    if k==0: ax.set_ylabel(r'$z$, mm')
    ax.set_xlabel(r'$r$, mm')
    if plot_regions:
        for r in rs[1:]:
            bx,by = rectangle(r.rmax,r.zmin,r.zmax,1e-3)
            ax.plot(bx,by,'w')
fig.savefig('regions.pdf')

#%%#################################################################################################
# Plot a detail at t = 25 ns (FAIL< DONT USE)
#from chemistry import nui_fun
#from streamer_2d_cyl import n_aver, n_avez, c_ave, c_dif
#simulation = simulation_load_barebones(rootdir + '/midpoint_fine/E_4MVm')
#simulation.E0 = 4e6
#ts = load_t_array(simulation)
#tplots = np.arange(25,26,.05)*1e-9
#kplots_double = np.interp(tplots,ts,np.arange(len(ts)))
#kplots = np.rint(kplots_double).astype(np.int)
#Nplots = len(kplots)
#
##%%
#fig,axs = plt.subplots(1,Nplots,figsize=(11,6))
#fig.tight_layout()
#scale = 1e-6
#uaxs=np.array([UpdateablePcolorAxis(axs[k],scale=scale,pseudo=False,
#                                    zoom=[(-.1e-3,.1e-3),(-1.75e-3,-1.5e-3)])
#    for k in range(Nplots)])
##%%
#plot_regions=False
#for k,kplot in enumerate(kplots):
#    rs = load_all_region_fields(simulation,kplot)
#    print(k,kplot)
#    print(rs)
#    system = rs[0]
#    assert system.name=='system'
#    #for r in [r for r in rs if r.level==2]:
#    for r in rs:
#        z_ea = c_ave(r.Ne.ea_x(1))
#        il_ea = c_dif(r.Ne,1)/c_ave(r.Ne,1)
#        plt.plot(z_ea[:],il_ea[0,:])
#        #E = np.sqrt(n_aver(r.Ercr)**2+n_avez(r.Ezcz)**2)
#        #nui = nui_fun(E)
#        #uaxs[k].plot(np.log10(nui*r.Ne+1),title=str(myfloat(ts[kplot]/1e-9))+' ns')
#        #uaxs[k].update_axes = True
#    ax = uaxs[k].ax
#    assert ax is axs[k]
#    if k==0: ax.set_ylabel(r'$z$, $\mu$m')
#    ax.set_xlabel(r'$r$, $\mu$m')
#    if plot_regions:
#        for r in rs[1:]:
#            bx,by = rectangle(r.rmax,r.zmin,r.zmax,scale)
#            ax.plot(bx,by,'w')

#%%#################################################################################################
# Plot a detail at t = 25 ns
from trump import c_ave, c_dif
simulation = simulation_load_barebones(rootdir + '/midpoint_fine/E_4MVm')
simulation.E0 = 4e6
ts = load_t_array(simulation)
tplots = np.arange(25.5,26,.1)*1e-9
kplots_double = np.interp(tplots,ts,np.arange(len(ts)))
kplots = np.rint(kplots_double).astype(np.int)
Nplots = len(kplots)

plt.figure(3,figsize=(6,3))
plt.clf()
colors = 'rgbcmy'
zmin = -2.2e-3
zmax = -1.75e-3
for k,kplot in enumerate(kplots):
    rs = load_all_region_fields(simulation,kplot)
    print(k,kplot)
    print(rs)
    system = rs[0]
    assert system.name=='system'
    #for r in [r for r in rs if r.level==2]:
    for r in rs:
        if r.level==2 and r.zmin < zmax and r.zmax > zmin:
            z_ea = c_ave(r.Ne.ea_x(1))
            il_ea = c_dif(r.Ne,1)/c_ave(r.Ne,1)
            line,=plt.plot(z_ea[1:end-1]/1e-3,il_ea[0,1:end-1]*1e-6,colors[k])
            line.set_label('t = '+str(myfloat(tplots[k]/1e-9)) + ' ns')
plt.xlim(zmin/1e-3,zmax/1e-3)
plt.grid('True')
plt.xlabel(r'$z$, mm')
plt.ylabel(r'$(dn_e/dz)/n_e$, ($\mu$m)$^{-1}$')
plt.gca().set_position([0.125,0.15,0.8,0.8])
plt.legend()
plt.savefig('gradne.pdf')